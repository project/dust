<?php
/**
 * @file
 * MDC textfield component template.
 *
 * Variables available:
 * - $wrapper_attributes: Attributes for text field wrapper element.
 * - $attributes: Attributes for input textfield.
 * - $label: Label.
 * - $label_attributes: Attributes for label element.
 * - $extra: Extra input for autocomplete.
 * - $outline: Boolean indicating whether or not to use outline theming.
 * - $full_width: Boolean indicating whether or not to use full width theming.
 * - $maxlength: Number indicating maximal allowed value length
 * - $help_text: Help text.
 * - $help_attributes: Attributes for help text.
 * - $icon: Icon name for leading icon.
 * - $trailing_icon: Icon name for trailing icon.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-textfield
 * @see template_preprocess_mdc_textfield()
 */
?>
<div class="dust-textfield">
    <div <?php print drupal_attributes($wrapper_attributes); ?>>
      <?php if (!empty($icon)): ?>
        <i class="material-icons mdc-text-field__icon"><?php print $icon; ?></i>
      <?php endif; ?>
      <?php if (!empty($trailing_icon)): ?>
        <i class="material-icons mdc-text-field__icon"><?php print $trailing_icon; ?></i>
      <?php endif; ?>
      <input <?php print drupal_attributes($attributes); ?>>
        <?php if (!empty($extra)) {
            print $extra;
        }
        ?>
      <?php if ($outline): ?>
        <div class="mdc-notched-outline">
          <div class="mdc-notched-outline__leading"></div>
          <div class="mdc-notched-outline__notch">
            <?php if (!empty($label)): ?>
              <label <?php print drupal_attributes($label_attributes); ?>> <?php print $label; ?> </label>
            <?php endif; ?>
          </div>
          <div class="mdc-notched-outline__trailing"></div>
        </div>
      <?php elseif ($full_width): ?>
        <div class="mdc-line-ripple"></div>
      <?php else: ?>
        <?php if (!empty($label)): ?>
          <label <?php print drupal_attributes($label_attributes); ?>> <?php print $label; ?> </label>
        <?php endif; ?>
        <div class="mdc-line-ripple"></div>
      <?php endif; ?>
    </div>
    <div class="mdc-text-field-helper-line">
      <?php if (!empty($help_text)): ?>
        <div <?php print drupal_attributes($help_attributes); ?>><?php print $help_text; ?></div>
      <?php endif; ?>
      <?php if (!empty($maxlength)): ?>
        <div class="mdc-text-field-character-counter">0 / <?php print $maxlength; ?></div>
      <?php endif; ?>
    </div>
</div>
