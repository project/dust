<?php
/**
 * @file mdc-top-app-bar.tpl.php
 * MDC top app bar component template.
 *
 * Variables available:
 * - $attributes: The mdc checkbox attributes.
 * - $topbar_start: Any markup to render at the start of
 *                  the app bar, such as icon button etc.
 * - $topbar_end: Any markup to render at the end of
 *                  the app bar, such as icon button etc.
 * - $fixed_adjust: Whether to add fixed adjust markup or not.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-top-app-bar
 */
?>
<div <?php print drupal_attributes($attributes); ?>>
  <div class="mdc-top-app-bar__row">
    <?php if (!empty($top_app_start)): ?>
      <div class="mdc-top-app-bar__section mdc-top-app-bar__section--align-start">
        <?php print render($top_app_start); ?>
        <div class="mdc-top-app-bar__title"><?php print $title; ?></div>
      </div>
    <?php endif; ?>
    <?php if (!empty($top_bar_end)): ?>
      <div class="mdc-top-app-bar__section mdc-top-app-bar__section--align-end">
        <?php print render($top_bar_end); ?>
      </div>
    <?php endif; ?>
  <?php if (!empty($top_app_content)): ?>
    <?php print render($top_app_content); ?>
  <?php endif; ?>
  </div>
</div>
<?php if ($fixed_adjust): ?>
  <div class="mdc-top-app-bar--fixed-adjust"></div>
<?php endif; ?>