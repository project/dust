<?php

/**
 * @file
 * MDC menu component template.
 *
 * Variables available:
 * - $links: An array containing these options per link:
 *   - text: Link text
 *   - path: Link path
 *   - options: Link options
 * - $menu_trigger: An Html string that opens the menu. It should has
 *    dust-mdc-menu-trigger class.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-menu
 */
?>
<div class="mdc-menu-surface--anchor">
  <?php if (!empty($menu_trigger)) : ?>
    <?php print $menu_trigger; ?>
  <?php endif; ?>
  <div <?php print drupal_attributes($menu_attributes); ?>>
    <ul <?php print drupal_attributes($list_attributes); ?>>
      <?php foreach ($links as $index => $link): ?>
        <li <?php print drupal_attributes($link['attributes'] ?? []); ?>>
          <a <?php print drupal_attributes($link['link_attributes'] ?? []); ?>>
            <?php print !empty($link['options']['html']) ? $link['text'] : check_plain($link['text']); ?>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
