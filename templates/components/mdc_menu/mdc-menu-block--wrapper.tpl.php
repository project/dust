<?php

/**
 * @file
 * MDC menu block wrapper template.
 *
 * Variables available:
 *   An associative array containing:
 *   - $tree: An HTML string containing the tree's items.
 *   - $menu_trigger: An Html string that opens the menu.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-menu
 */
?>
<div class="mdc-menu-surface--anchor">
  <ul role="menu" class="mdc-list mdc-menu-surface">
    <?php print $tree; ?>
  </ul>
  <?php if (!empty($menu_trigger)) : ?>
    <?php print $menu_trigger; ?>
  <?php endif; ?>
</div>
