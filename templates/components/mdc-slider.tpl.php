
<?php
/**
 * @file mdc-slider.tpl.php
 * MDC slider component template.
 *
 * Variables available:
 * - $attributes: The mdc slider attributes.
 * - $display_markers: Whether to display markers or not.
 * - $discrete: Whether slider is discrete or not.
 * - $input_attributes: Attributes for hidden input.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-slider
 */
?>
<div tabindex="0" role="slider" <?php print drupal_attributes($attributes); ?>>
  <div class="mdc-slider__track-container">
    <div class="mdc-slider__track"></div>
    <?php if ($display_markers): ?>
      <div class="mdc-slider__track-marker-container"></div>
    <?php endif; ?>
  </div>
  <div class="mdc-slider__thumb-container">
    <?php if ($discrete): ?>
      <div class="mdc-slider__pin">
        <span class="mdc-slider__pin-value-marker"></span>
      </div>
    <?php endif; ?>
    <svg class="mdc-slider__thumb" width="21" height="21">
      <circle cx="10.5" cy="10.5" r="7.875"></circle>
    </svg>
    <div class="mdc-slider__focus-ring"></div>
  </div>
</div>
<input <?php print drupal_attributes($input_attributes); ?>>
