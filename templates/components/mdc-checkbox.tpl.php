<?php

/**
 * @file
 * MDC checkbox component template.
 *
 * Variables available:
 * - $attributes: The mdc checkbox attributes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-checkbox
 */
?>
<div class="mdc-checkbox mdc-ripple-surface">
  <input <?php print drupal_attributes($attributes); ?>/>
  <div class="mdc-checkbox__background">
    <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24" focusable="false">
      <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"></path>
    </svg>
    <div class="mdc-checkbox__mixedmark"></div>
  </div>
</div>
