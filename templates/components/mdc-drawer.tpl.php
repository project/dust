<?php

/**
 * @file
 * MDC drawer component template.
 *
 * Variables available:
 * - $type: The drawer type: permanent, dismissible, modal.
 * - $title: The title to use for drawer primary heading.
 * - $subtitle: The subtitle to use for drawer primary heading.
 * - $content: The drawer content.
 * - $title_tag: The title wrapper tag.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-drawer
 */
?>
<aside <?php print drupal_attributes($attributes); ?>>
    <?php if (!empty($title) || !empty($subtitle)): ?>
    <div class="mdc-drawer__header">
        <?php if (!empty($title)): ?>
            <<?php print $title_tag; ?> class="mdc-drawer__title"><?php print $title ?></<?php print $title_tag; ?>>
        <?php endif; ?>
        <?php if (!empty($subtitle)): ?>
            <h6 class="mdc-drawer__subtitle"><?php print $subtitle ?></h6>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div class="mdc-drawer__content" <?php if (isset($content_direction)): ?>dir="<?php  print $content_direction;  ?>"<?php endif; ?>>
       <?php print $content; ?>
    </div>
</aside>
<?php if ($type === 'modal'): ?>
    <div class="mdc-drawer-scrim"></div>
<?php endif; ?>
