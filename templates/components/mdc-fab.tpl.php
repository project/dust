<?php
/**
 * @file mdc-fab.tpl.php
 * MDC fab component template.
 *
 * Variables available:
 * - $attributes: Additional mdc icon button attributes.
 * - $leading: The leading fab html.
 * - $trailing: The trailing fab html.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-fab
 */
?>
<button <?php print drupal_attributes($attributes); ?>>
  <div class="mdc-fab__ripple"></div>
  <?php if (!empty($leading)): ?>
    <?php print $leading; ?>
  <?php endif; ?>
  <?php if (!empty($trailing)): ?>
    <?php print $trailing; ?>
  <?php endif; ?>
</button>