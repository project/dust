<?php
/**
 * @file mdc-snackbar.tpl.php
 * MDC snackbar component template.
 *
 * Variables available:
 * - $icon: Any prefix icon markup to use.
 * - $message: The snackbar message.
 * - $actions: Snackbar actions for example buttons
 *
 * To initiate JS we must correct classes see link below.
 * Example 'mdc-snackbar__dismiss' to dismiss snackbar.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-snackbar
 */
?>
<div <?php print drupal_attributes($attributes); ?>>
  <div class="mdc-snackbar__surface">
    <?php if (isset($icon)): ?>
      <?php print $icon; ?>
    <?php endif; ?>
    <div class="mdc-snackbar__label" role="status" aria-live="polite">
      <span class="mdc-typography--body1"><?php print t($message); ?></span>
    </div>
    <?php if (!empty($actions)): ?>
      <div class="mdc-snackbar__actions">
        <?php print $actions; ?>
      </div>
    <?php endif; ?>
  </div>
</div>
