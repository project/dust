<?php
/**
 * @file mdc-icon-button.tpl.php
 * MDC icon button component template.
 *
 * Variables available:
 * - $attributes: Additional mdc icon button attributes.
 * - $icon: The mdc icon to use for example 'Favourite.
 * - $toggle_icon: The mdc toggle icon to use if any for example 'Favourite.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-icon-button
 */
?>
<button <?php print drupal_attributes($attributes);?>>
  <?php if (!empty($icon)): ?>
    <i class="material-icons mdc-icon-button__icon"><?php print $icon; ?></i>
  <?php endif; ?>
  <?php if (!empty($toggle_icon)): ?>
    <i class="material-icons mdc-icon-button__icon"><?php print $toggle_icon; ?></i>
  <?php endif; ?>
</button>