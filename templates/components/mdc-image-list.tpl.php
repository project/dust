<?php

/**
 * @file
 * MDC image list component template.
 *
 * Variables available:
 * - $attributes: Image list attributes.
 * - $items: The mdc image list attributes.
 * @code
 *   '0 => array(
 *     'image' => 'The theme image markup',
 *     'label' => 'Caption image'
 *   );
 * @code
 * - $attributes: The mdc image list attributes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-image-list
 */
?>
<ul <?php print drupal_attributes($attributes); ?>>
  <?php foreach ($items as $item): ?>
    <li class="mdc-image-list__item" role="listitem">
      <figure class="image-list-aspect-container mdc-image-list__image-aspect-container">
        <?php if (!empty($item['image'])): ?>
          <?php print $item['image']; ?>
        <?php endif; ?>
        <?php if (!empty($item['label'])): ?>
          <figcaption class="mdc-image-list__supporting">
            <span class="mdc-image-list__label">
              <?php print $item['label']; ?>
            </span>
          </figcaption>
        <?php endif; ?>
      </figure>
    </li>
  <?php endforeach; ?>
</ul>
