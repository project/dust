<?php

/**
 * @file
 * MDC select component template.
 *
 * Variables available:
 * - $attributes: The mdc checkbox attributes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-select
 */
?>
<div <?php print drupal_attributes($element['#attributes']); ?>>
  <div <?php print drupal_attributes($anchor_attributes); ?>>
    <i class="mdc-select__dropdown-icon"></i>
    <?php if (!empty($outlined)): ?>
      <div class="mdc-notched-outline">
        <div class="mdc-notched-outline__leading"></div>
        <?php if (!empty($element['#title'])): ?>
          <div class="mdc-notched-outline__notch">
            <label <?php print drupal_attributes($label_attributes); ?>>
              <?php print $element['#title']; ?>
            </label>
          </div>
        <?php endif; ?>
        <div class="mdc-notched-outline__trailing"></div>
      </div>
    <?php else: ?>
      <?php if (!empty($element['#title'])): ?>
        <label <?php print drupal_attributes($label_attributes); ?>>
          <?php print $element['#title']; ?>
        </label>
      <?php endif; ?>
      <div class="mdc-line-ripple"></div>
    <?php endif; ?>
    <div <?php print drupal_attributes($selected_text_attributes); ?> role="button" aria-haspopup="listbox" <?php if (!empty($element['#title'])): ?>
      aria-labelledby="<?php print $label_attributes['id']; ?> <?php print $selected_text_attributes['id']; ?>"<?php endif; ?>>
      <?php if (isset($selected)): ?>
        <?php print check_plain($selected); ?>
      <?php endif; ?>
    </div>
  </div>

  <div class="mdc-select__menu mdc-menu mdc-menu-surface" role="listbox">
  </div>

  <?php print $select; ?>
</div>
