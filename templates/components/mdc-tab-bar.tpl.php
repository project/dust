<?php

/**
 * @file
 * MDC tab bar component template.
 *
 * Variables available:
 * - $attributes: The mdc tab bar attributes.
 * - $stacked: Whether tabs is stacked or not.
 * - $tabs: An array of tabs. Each $tab in $tabs contains:
 *   - $tab['attributes']: The tab attributes.
 *   - $tab['label']: The tab label.
 *   - $tab['icon']: The tab icon.
 *   - $tab['active']: Whether tab is active or not.
 *   - $tab['indicator_classes']: The indicator classes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-tab-bar
 * @see template_preprocess_mdc_tab_bar()
 */
?>
<div <?php print drupal_attributes($attributes); ?>>
    <div class="mdc-tab-scroller">
        <div class="mdc-tab-scroller__scroll-area">
            <div class="mdc-tab-scroller__scroll-content">
              <?php foreach ($tabs as $tab): ?>
                <?php if (isset($tab['link'])): ?>
                <div <?php print drupal_attributes($tab['attributes']) ?>>
                    <span class="mdc-tab__content">
                  <?php print $tab['link'] ?>
                    </span>
                    <span class="<?php print $tab['indicator_classes'] ?>">
                            <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                        </span>
                    <span class="mdc-tab__ripple"></span>
                </div>
                <?php else: ?>
                      <button <?php print drupal_attributes($tab['attributes']) ?>>
                        <span class="mdc-tab__content">
                            <?php if (!empty($tab['icon'])): ?>
                                <span class="mdc-tab__icon material-icons" aria-hidden="true"><?php print $tab['icon'] ?></span>
                            <?php endif; ?>
                          <?php if (!empty($tab['label'])): ?>
                              <span class="mdc-tab__text-label"><?php print $tab['label'] ?></span>
                          <?php endif; ?>
                        </span>
                          <span class="<?php print $tab['indicator_classes'] ?>">
                            <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                        </span>
                          <span class="mdc-tab__ripple"></span>
                      </button>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
