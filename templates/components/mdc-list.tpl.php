<?php

/**
 * @file mdc-list.tpl.php
 *
 * MDC list component template.
 *
 * Variables available:
 * - $items: An associative array example:
 * @code
 *   '0 => array(
 *     'text' => 'Inbox',
 *     'secondary_text' => 'Check your inbox',
 *     'icon_lead' => 'favourite',
 *     'icon_trail' => 'star',
 *   );
 * @code
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-list
 */
?>
<?php if (!empty($items)): ?>
  <<?php print $type ?> <?php print drupal_attributes($attributes); ?>>
    <?php foreach ($items as $key => $item): ?>
      <li <?php print drupal_attributes($item['attributes']); ?>>
        <?php if (!empty($item['icon_lead'])): ?>
          <span class="mdc-list-item__graphic material-icons" aria-hidden="true">
            <?php print $item['icon_lead']; ?>
          </span>
        <?php endif; ?>
        <?php if (!empty($item['markup'])): ?>
          <?php print $item['markup']; ?>
        <?php endif; ?>
        <?php if (!empty($item['text']) || !empty($item['secondary_text'])): ?>
          <span class="mdc-list-item__text">
            <span class="mdc-list-item__primary-text"><?php print $item['text']; ?></span>
            <?php if (!empty($item['secondary_text'])): ?>
              <span class="mdc-list-item__secondary-text"><?php print $item['secondary_text']; ?></span>
            <?php endif; ?>
          </span>
        <?php endif; ?>
        <?php if (!empty($item['icon_trail'])): ?>
          <span class="mdc-list-item__meta mdc-list-item__graphic material-icons" aria-hidden="true">
            <?php print $item['icon_trail']; ?>
          </span>
        <?php endif; ?>
      </li>
    <?php if ($divider && $key !== array_key_last($items)): ?>
      <li <?php print drupal_attributes($divider_attributes); ?>></li>
    <?php endif; ?>
    <?php endforeach; ?>
  </<?php print $type ?>>
<?php endif; ?>
