<?php

/**
 * @file
 * MDC linear progress bar component template (indeterminate variant).
 *
 * Variables available:
 * - $percent: The percentage value of progress.
 * - $label: The progress label.
 * - $attributes: Any attributes too add.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-linear-progress
 */
?>
<div <?php print drupal_attributes($attributes); ?>>
  <?php if (!empty($label)): ?>
    <div class="progress-help-text mdc-typography--body2"><?php print $label; ?></div>
  <?php endif; ?>
  <nav
    role="progressbar"
    class="dust-mdc-linear-progress mdc-linear-progress mdc-linear-progress--indeterminate"
    aria-valuemin="0"
    aria-valuemax="100"
    <?php if (!empty($label)): ?>
      aria-valuetext="<?php print $label; ?>"
    <?php endif; ?>
  >
    <div class="mdc-linear-progress__buffering-dots"></div>
    <div class="mdc-linear-progress__buffer"></div>
    <div class="mdc-linear-progress__bar mdc-linear-progress__primary-bar">
      <span class="mdc-linear-progress__bar-inner"></span>
    </div>
    <div class="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">
      <div class="mdc-linear-progress__bar-inner"></div>
    </div>
  </nav>
</div>
