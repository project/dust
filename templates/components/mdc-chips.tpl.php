<?php

/**
 * @file
 * MDC chip component template.
 *
 * Variables available:
 * - $attributes: The mdc checkbox attributes.
 * - $items: An array of chip items to render example:
 * @code
 *   $items = array(
 *     0 => array(
 *       'leading_icon' => 'beer',
 *       'text' => 'Beer',
 *       'trailing_icon' => 'close',
 *     );
 *   );
 * @code
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-chips
 */
?>
<ul <?php print drupal_attributes($attributes); ?>>
  <?php foreach ($items as $item): ?>
    <li tabindex="0" class="mdc-ripple-surface mdc-chip">
      <?php if ($item['leading_icon']): ?>
        <i class="mdc-chip__icon mdc-chip__icon--leading material-icons" aria-hidden="true"><?php print $item['leading_icon']; ?></i>
      <?php endif; ?>
      <?php if (!empty($item['markup'])): ?>
        <?php print $item['markup']; ?>
      <?php endif; ?>
      <?php if (!empty($item['text'])): ?>
        <div class="mdc-chip__text"><?php print drupal_ucfirst($item['text']); ?></div>
      <?php endif; ?>
      <?php if ($item['trailing_icon']): ?>
        <i class="mdc-chip__icon mdc-chip__icon--trailing material-icons" aria-hidden="true"><?php print $item['trailing_icon']; ?></i>
      <?php endif; ?>
    </li>
  <?php endforeach; ?>
</ul>
