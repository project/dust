
<?php
/**
 * @file mdc-switch.tpl.php
 * MDC switch component template.
 *
 * Variables available:
 * - $attributes: The mdc switch attributes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-switch
 */
?>
<div class="mdc-switch">
  <div class="mdc-switch__track"></div>
  <div class="mdc-switch__thumb-underlay">
    <div class="mdc-switch__thumb"></div>
    <input <?php print drupal_attributes($attributes); ?>/>
  </div>
  <div class="mdc-switch__knob"></div>
</div>