<?php
/**
 * @file mdc-radio.tpl.php
 * MDC checkbox component template.
 *
 * Variables available:
 * - $attributes: The mdc radio attributes.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-radio
 */
?>
<div class="mdc-radio mdc-ripple-surface">
  <input <?php print drupal_attributes($attributes); ?>/>
  <div class="mdc-radio__background">
    <div class="mdc-radio__outer-circle"></div>
    <div class="mdc-radio__inner-circle"></div>
  </div>
</div>
