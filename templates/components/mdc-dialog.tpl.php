<?php

/**
 * @file mdc-dialog.tpl.php
 * MDC dialog component template.
 *
 * Variables available:
 * - $attributes: The mdc checkbox attributes.
 * - $title: The mdc checkbox attributes.
 * - $content: The mdc checkbox attributes.
 * - $actions: Any dialog actions markup to render this
 *             could be for example buttons.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-dialog
 */
?>
<div <?php print drupal_attributes($attributes); ?>>
    <div class="mdc-dialog__container">
        <div class="mdc-dialog__surface">
          <?php if (!empty($title)): ?>
              <h2 class="mdc-dialog__title" id="<?php print $id ?>-title"><?php print $title; ?></h2>
          <?php endif; ?>
          <?php if (!empty($content)): ?>
              <div class="mdc-dialog__content" id="<?php print $id ?>-content"><?php print $content; ?></div>
          <?php endif; ?>
          <?php if (!empty($actions)): ?>
              <footer class="mdc-dialog__actions">
                <?php print render($actions); ?>
              </footer>
          <?php endif; ?>
        </div>
    </div>
    <div class="mdc-dialog__scrim"></div>
</div>
