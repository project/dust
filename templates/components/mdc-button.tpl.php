<?php
/**
 * @file mdc-button.tpl.php
 * MDC button component template.
 *
 * Variables available:
 * - $attributes: Additional mdc button classes for example 'mdc-button--outlined'.
 * - $icon: The mdc icon to use for example 'Favourite'.
 * - $label: The button label.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-button
 */
?>
<button <?php print drupal_attributes($attributes); ?>>
  <?php if (!empty($icon)): ?>
    <i class="material-icons mdc-button__icon" aria-hidden="true"><?php print $icon; ?></i>
  <?php endif; ?>
  <span class="mdc-button__label"><?php print t($label); ?></span>
</button>