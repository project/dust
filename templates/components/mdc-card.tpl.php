<?php

/**
 * @file
 * MDC card component template.
 *
 * Variables available:
 * - $attributes: The mdc card attributes.
 * - $image_url: The image url to use for card media.
 * - $image_title: The image title to use for card media title.
 * - $title: The title to use for card primary heading.
 * - $subtitle: The subtitle to use for primary card heading.
 * - $body: The body to use for mdc card.
 * - $action_buttons: Mdc action buttons to use. Example use:
 *   @code
 *     array(
 *       'Read',
 *     );
 *   @endcode
 * - $action_icons: Mdc action icons to use. Example use:
 *   @code
 *     array(
 *       'Share' => 'share',
 *     );
 *   @endcode
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-card
 */
?>
<div<?php print drupal_attributes($attributes) ?>>
  <div class="mdc-card__primary-action">
      <?php if (!empty($image_url)): ?>
    <div class="mdc-card__media mdc-card__media--16-9" style="background-image: url(<?php print $image_url; ?>)">
      <div class="mdc-card__media-content"><?php print $image_title; ?></div>
    </div>
      <?php endif; ?>
      <?php if (!empty($title) || !empty($subtitle)): ?>
    <div class="mdc-card__primary">
      <?php if (!empty($title)): ?>
        <h2 class="mdc-typography mdc-typography--headline6"><?php print $title; ?></h2>
      <?php endif; ?>
      <?php if (!empty($subtitle)): ?>
        <h3 class="mdc-typography mdc-typography--subtitle2"><?php print $subtitle; ?></h3>
      <?php endif; ?>
    </div>
      <?php endif; ?>
    <?php if (!empty($body)): ?>
      <div class="mdc-card__secondary mdc-typography mdc-typography--body2"><?php print $body; ?></div>
    <?php endif; ?>
  </div>
  <div class="mdc-card__actions">
    <?php if (!empty($action_buttons)): ?>
      <div class="mdc-card__action-buttons">
        <?php foreach ($action_buttons as $button_label): ?>
          <button class="mdc-button mdc-card__action mdc-card__action--button">
            <span class="mdc-button__label"><?php print $button_label; ?></span>
          </button>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    <?php if (!empty($action_icons)): ?>
      <div class="mdc-card__action-icons">
        <?php foreach ($action_icons as $title => $name): ?>
          <button class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon" title="<?php print $title; ?>">
            <?php print $name; ?>
          </button>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
  </div>
</div>
