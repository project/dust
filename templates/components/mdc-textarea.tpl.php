<?php
/**
 * @file
 * MDC textarea component template.
 *
 * Variables available:
 * - $value: Textarea value.
 * - $wrapper_attributes: Attributes for text field wrapper element.
 * - $attributes: Attributes for textarea.
 * - $label: Label.
 * - $label_attributes: Attributes for label element.
 * - $maxlength: Number indicating maximal allowed value length
 * - $help_text: Help text.
 * - $help_attributes: Attributes for help text.
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-textfield
 * @see template_preprocess_mdc_textfield()
 */
?>
<div <?php print drupal_attributes($wrapper_attributes); ?>>
  <?php if (!empty($maxlength)): ?>
    <div class="mdc-text-field-character-counter">0 / <?php print $maxlength; ?></div>
  <?php endif; ?>
  <?php if (!empty($resizable)): ?>
    <span class="mdc-text-field__resizer">
  <?php endif; ?>
  <?php if (!empty($outlined)): ?>
    <textarea <?php print drupal_attributes($attributes); ?>><?php print $value; ?></textarea>
    <div class="mdc-notched-outline">
      <div class="mdc-notched-outline__leading"></div>
      <?php if (!empty($label)): ?>
        <div class="mdc-notched-outline__notch">
          <label <?php print drupal_attributes($label_attributes); ?>> <?php print $label; ?> </label>
        </div>
      <?php endif; ?>
      <div class="mdc-notched-outline__trailing"></div>
    </div>
  <?php else: ?>
    <span class="mdc-text-field__ripple"></span>
    <textarea <?php print drupal_attributes($attributes); ?>><?php print $value; ?></textarea>
    <span class="mdc-line-ripple"></span>
  <?php endif; ?>
</div>
<?php if (!empty($help_text)): ?>
  <div class="mdc-text-field-helper-line">
    <div <?php print drupal_attributes($help_attributes); ?>><?php print $help_text; ?></div>
  </div>
<?php endif; ?>
