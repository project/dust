/**
 * @file
 * Javascript required for dust theme to integrate with MDC web.
 */

(function ($, Drupal) {
  'use strict';

  // Make sure our objects are defined.
  if (!Drupal.MDC && typeof mdc !== 'undefined') {
    Drupal.MDC = Drupal.MDC || {};
  }

  /**
   * Object to integrate drupal with MDC web components.
   *
   * This object will contain methods used by MDC making it
   * easier to integrate our theme functions with component
   * effects.
   */
  Drupal.MDC = {

    /**
     * MDC ripple effect.
     *
     * As used with button components.
     */
    ripple: function (element) {
      if (typeof mdc.ripple.MDCRipple !== 'undefined') {
        return mdc.ripple.MDCRipple.attachTo(element);
      }
    },

    /**
     * MDC checkbox JS.
     */
    checkbox: function (element) {
      if (typeof mdc.checkbox.MDCCheckbox !== 'undefined') {
        return mdc.checkbox.MDCCheckbox.attachTo(element);
      }
    },

    /**
     * MDC chip.
     */
    chip: function (id) {
      if (typeof mdc.checkbox.MDCChip !== 'undefined') {
        mdc.chips.MDCChip.attachTo(document.querySelector(id));
      }
    },

    /**
     * MDC chip set.
     */
    chipSet: function (element) {
      if (typeof mdc.chips.MDCChipSet !== 'undefined') {
        var chipSet = new mdc.chips.MDCChipSet.attachTo(element);

        // Remove if trailing icon is clicked.
        chipSet.listen('MDCChip:removal', function (event) {
          this.removeChild(event.detail.root);
        });
        return chipSet;
      }
    },

    /**
     * MDC data table.
     */
    dataTable: function (element) {
      if (typeof mdc.dataTable.MDCDataTable !== 'undefined') {
        mdc.dataTable.MDCDataTable.attachTo(element);
      }
    },

    /**
     * MDC dialog.
     */
    dialog: function (element) {
      if (typeof mdc.dialog.MDCDialog !== 'undefined') {
        return mdc.dialog.MDCDialog.attachTo(element);
      }
    },

    /**
     * MDC drawer.
     */
    drawer: function (element) {
      if (typeof mdc.drawer.MDCDrawer !== 'undefined') {
        return mdc.drawer.MDCDrawer.attachTo(element);
      }
    },

    /**
     * MDC floating label.
     */
    floatingLabel: function (id) {
      if (typeof mdc.floatingLabel.MDCFloatingLabel !== 'undefined') {
        mdc.floatingLabel.MDCFloatingLabel.attachTo(document.querySelector(id));
      }
    },

    /**
     * MDC form field.
     */
    formField: function (element) {
      if (typeof mdc.formField.MDCFormField !== 'undefined') {
        return mdc.formField.MDCFormField.attachTo(element);
      }
    },

    /**
     * MDC Icon button toggle.
     */
    iconButtonToggle: function (element) {
      if (typeof mdc.iconButton.MDCIconButtonToggle !== 'undefined') {
        mdc.iconButton.MDCIconButtonToggle.attachTo(element);
      }
    },

    /**
     * MDC Line ripple effect.
     */
    lineRipple: function (element) {
      if (typeof mdc.lineRipple.MDCLineRipple !== 'undefined') {
        return mdc.lineRipple.MDCLineRipple.attachTo(element);
      }
    },

    /**
     * MDC Liner progress.
     */
    linerProgress: function (element) {
      if (typeof mdc.linearProgress.MDCLinearProgress !== 'undefined') {
        return mdc.linearProgress.MDCLinearProgress.attachTo(element);
      }
    },

    /**
     * MDC List.
     */
    list: function (element) {
      if (typeof mdc.list.MDCList !== 'undefined') {
        return mdc.list.MDCList.attachTo(element);
      }
    },

    /**
     * MDC Menu.
     */
    menu: function (element) {
      if (typeof mdc.menu.MDCMenu !== 'undefined') {
        var $element = $(element);
        if ($element.data('MDCMenu') !== undefined) {
          return $element.data('MDCMenu');
        }
        var menu = mdc.menu.MDCMenu.attachTo(element);
        $element.data('MDCMenu', menu);
        return menu;
      }
    },

    /**
     * MDC Menu surface.
     */
    menuSurface: function (id) {
      if (typeof mdc.menuSurface.MDCMenuSurface !== 'undefined') {
        mdc.menuSurface.MDCMenuSurface.attachTo(document.querySelector(id));
      }
    },

    /**
     * MDC Notched outline.
     */
    notchedOutline: function (id) {
      if (typeof mdc.notchedOutline.MDCNotchedOutline !== 'undefined') {
        mdc.notchedOutline.MDCNotchedOutline.attachTo(document.querySelector(id));
      }
    },

    /**
     * MDC Radio.
     */
    radio: function (element) {
      if (typeof mdc.radio.MDCRadio !== 'undefined') {
        return mdc.radio.MDCRadio.attachTo(element);
      }
    },

    /**
     * MDC Select.
     */
    select: function ($element) {
      if (typeof mdc.select.MDCSelect !== 'undefined') {
        if ($element.data('MDCSelect') !== undefined) {
          return $element.data('MDCSelect');
        }
        var select = mdc.select.MDCSelect.attachTo($element[0]);
        $element.data('MDCSelect', select);
        return select;
      }
    },

    /**
     * MDC Slider.
     */
    slider: function (element) {
      if (typeof mdc.slider.MDCSlider !== 'undefined') {
        return mdc.slider.MDCSlider.attachTo(element);
      }
    },

    /**
     * MDC Snackbar.
     */
    snackbar: function (id) {
      if (typeof mdc.snackbar.MDCSnackbar !== 'undefined') {
        mdc.snackbar.MDCSnackbar.attachTo(document.querySelector(id));
      }
    },

    /**
     * MDC Switch.
     */
    switch: function ($element) {
      if (typeof mdc.switchControl.MDCSwitch !== 'undefined') {
        mdc.switchControl.MDCSwitch.attachTo($element[0]);
      }
    },

    /**
     * MDC Tab bar.
     */
    tabBar: function ($element) {
      if (typeof mdc.tabBar.MDCTabBar !== 'undefined') {
        if ($element.data('MDCTabBar') !== undefined) {
          return $element.data('MDCTabBar');
        }
        var tabBar = mdc.tabBar.MDCTabBar.attachTo($element[0]);
        $element.data('MDCTabBar', tabBar);
        return tabBar;
      }
    },

    /**
     * MDC Text field.
     */
    textField: function ($element) {
      if (typeof mdc.textField.MDCTextField !== 'undefined') {
        if ($element.data('MDCTextField') !== undefined) {
          return $element.data('MDCTextField');
        }
        var textfield = mdc.textField.MDCTextField.attachTo($element[0]);
        $element.data('MDCTextField', textfield);
        return textfield;
      }
    },

    /**
     * MDC Top app bar.
     */
    topAppBar: function (id) {
      if (typeof mdc.topAppBar.MDCTopAppBar !== 'undefined') {
        mdc.topAppBar.MDCTopAppBar.attachTo(document.querySelector(id));
      }
    }
  };

})(jQuery, Drupal);
