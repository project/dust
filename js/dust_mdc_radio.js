(function ($, Drupal) {
  "use strict";

  /**
   * Attach MDC checkbox and ripple effect.
   */
  Drupal.behaviors.DustMdcRadio = {
    attach: function (context) {
      $('.mdc-radio', context).once('mdc-radio', function () {
        var radio = new Drupal.MDC.radio(this),
        radioField = $(this).closest('.mdc-form-field').closest('.form-type-radio')[0];
        if (typeof radioField !== 'undefined') {
          var formfield = new Drupal.MDC.formField(radioField);
          formfield.input = radio;
        }
      });
    }
  }
})(jQuery, Drupal);
