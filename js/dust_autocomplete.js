(function ($) {

  /**
   * Attaches the autocomplete behavior to all required fields.
   */
  Drupal.behaviors.autocomplete = {
    attach: function (context, settings) {
      var acdb = [];
      $('input.autocomplete', context).once('autocomplete', function () {
        var uri = this.value;
        if (!acdb[uri]) {
          acdb[uri] = new Drupal.ACDB(uri);
        }
        var $input = $('#' + this.id.substr(0, this.id.length - 13))
          .attr('autocomplete', 'OFF')
          .attr('aria-autocomplete', 'list');
        $($input[0].form).submit(Drupal.autocompleteSubmit);
        $input.parent()
          .attr('role', 'application')
          .append(
            $('<span class="element-invisible" aria-live="assertive"></span>')
              .attr('id', $input.attr('id') + '-autocomplete-aria-live')
          );
        new Drupal.jsAC($input, acdb[uri]);
      });
    }
  };

  /**
   * Prevents the form from submitting if the suggestions popup is open
   * and closes the suggestions popup when doing so.
   */
  Drupal.autocompleteSubmit = function () {
    return $('#autocomplete').each(function () {
      this.owner.hidePopup();
    }).length == 0;
  };

  /**
   * An AutoComplete object.
   */
  Drupal.jsAC = function ($input, db) {
    var ac = this;
    this.input = $input[0];
    this.ariaLive = $('#' + this.input.id + '-autocomplete-aria-live');
    this.db = db;

    $input
      .keydown(function (event) { return ac.onkeydown(this, event); })
      .keyup(function (event) { ac.onkeyup(this, event); })
      .blur(function () {
        setTimeout(function () {
          // Fix for IE 11 moving focus out of textfield when scrolling menu.
          if (!$(document.activeElement).closest('#autocomplete').length) {
            ac.hidePopup();
            ac.db.cancel();
          }
        }, 0);
      });
  };

  /**
   * Handler for the "keydown" event.
   */
  Drupal.jsAC.prototype.onkeydown = function (input, e) {
    if (!e) {
      e = window.event;
    }
    switch (e.keyCode) {
      case 40: // down arrow.
        this.selectDown();
        return false;
      case 38: // up arrow.
        this.selectUp();
        return false;
      case 13: // Enter.
        this.hidePopup(this);
        return false;
      case 27: // Esc.
        if (!$(e.target).is('input.form-autocomplete')) {

          // If focus is on menu item move it to textfield to allow change keyword.
          $(this.input).trigger('focus');
          return false;
        }
        else {
          return true;
        }

      default: // All other keys.
        return true;
    }
  };

  /**
   * Handler for the "keyup" event.
   */
  Drupal.jsAC.prototype.onkeyup = function (input, e) {
    if (!e) {
      e = window.event;
    }
    switch (e.keyCode) {
      case 16: // Shift.
      case 17: // Ctrl.
      case 18: // Alt.
      case 20: // Caps lock.
      case 33: // Page up.
      case 34: // Page down.
      case 35: // End.
      case 36: // Home.
      case 37: // Left arrow.
      case 38: // Up arrow.
      case 39: // Right arrow.
      case 40: // Down arrow.
        return true;

      case 13: // Enter.
        return false;

      case 9:  // Tab.
      case 27: // Esc.
      case 32: // Space.
        this.hidePopup(e.keyCode);
        return true;

      default: // All other keys.
        if (input.value.length > 0 && !input.readOnly) {
          this.populatePopup();
        }
        else {
          this.hidePopup(e.keyCode);
        }
        return true;
    }
  };

  /**
   * Puts the currently highlighted suggestion into the autocomplete field.
   */
  Drupal.jsAC.prototype.select = function (node) {
    this.input.value = $(node).data('autocompleteValue');
    $(this.input).trigger('autocompleteSelect', [node]);
  };

  /**
   * Highlights the next suggestion.
   */
  Drupal.jsAC.prototype.selectDown = function () {
    if (this.selected && this.selected.nextSibling) {
      this.highlight(this.selected.nextSibling, true);
    }
    else if (this.popup) {
      var lis = $('li', this.popup);
      if (lis.length > 0) {
        this.highlight(lis.get(0), true);
      }
    }
  };

  /**
   * Highlights the previous suggestion.
   */
  Drupal.jsAC.prototype.selectUp = function () {
    if (this.selected && this.selected.previousSibling) {
      this.highlight(this.selected.previousSibling, true);
    }
  };

  /**
   * Highlights a suggestion.
   *
   * @param node
   *   List item that suppose to be highlighted.
   * @param focus
   *   Whether or not to focus on element using MDC Menu.
   */
  Drupal.jsAC.prototype.highlight = function (node, focus) {
    if (this.selected) {
      $(this.selected).removeClass('selected');
    }
    $(node).addClass('selected');
    this.selected = node;
    $(this.ariaLive).html($(this.selected).html());
    if (typeof focus !== 'undefined' && focus) {
      this.mdcMenu.foundation_.adapter_.focusItemAtIndex($(node).index());
    }
  };

  /**
   * Unhighlights a suggestion.
   *
   * @param node
   *   List item that suppose to be highlighted.
   * @param focus
   *   Whether or not to focus on element using MDC Menu.
   */
  Drupal.jsAC.prototype.unhighlight = function (node, focus) {
    $(node).removeClass('selected');
    this.selected = false;
    $(this.ariaLive).empty();
    if (typeof focus !== 'undefined' && focus) {
      this.mdcMenu.foundation_.adapter_.focusListRoot();
    }
  };

  /**
   * Hides the autocomplete suggestions.
   */
  Drupal.jsAC.prototype.hidePopup = function (keycode) {
    // Select item if the right key or mousebutton was pressed.
    if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
      this.select(this.selected);
    }
    // Hide popup.
    var popup = this.popup;
    if (popup) {
      this.popup = null;
      this.mdcMenu = null;
      $(popup).remove();
    }
    $(this.ariaLive).text(Drupal.t('!selected selected.', {
      '!selected': $(this.selected).text()
    }));
    this.selected = false;
    $(this.input).attr('aria-expanded', false);
  };

  /**
   * Positions the suggestions popup and starts a search.
   */
  Drupal.jsAC.prototype.populatePopup = function () {
    var $input = $(this.input);
    var $parentMdcTextfield = $input.closest('.mdc-text-field');

    // Show popup.
    if (this.popup) {
      $(this.popup).remove();
    }
    this.selected = false;
    this.popup = $('<div id="autocomplete" class="mdc-menu mdc-menu-surface mdc-menu-surface--open"></div>')[0];
    this.popup.owner = this;

    $parentMdcTextfield.before(this.popup);

    // Do search.
    this.db.owner = this;
    this.db.search(this.input.value);
  };

  /**
   * Fills the suggestion popup with any matches received.
   */
  Drupal.jsAC.prototype.found = function (matches) {
    // If no value in the textfield, do not show the popup.
    if (!this.input.value.length) {
      return false;
    }

    // Prepare matches.
    var $input = $(this.input);
    var field_label_id = $input.siblings('label').attr('id');
    var ul = $('<ul role="listbox" class="mdc-list"></ul>');
    if ((typeof field_label_id !== 'undefined') && field_label_id.length) {
      ul.attr('aria-labelledby', field_label_id);
    }
    var ac = this;
    var matchesLength = 0;
    for (key in matches) {
      var item = matches[key];
      var label = '';
      if (item.indexOf('.mdc-list-item__text') !== -1) {
        label = $(item).find('.mdc-list-item__text').text();
      }
      else {
        label = item;
      }
      var listItem = $('<li tabindex="0" role="option" class="mdc-list-item"></li>');
      listItem
        .attr('aria-label', label)
        .html(item)
        .mousedown(function () { ac.hidePopup(this); })
        .mouseover(function () { ac.highlight(this, false); })
        .mouseout(function () { ac.unhighlight(this, false); })
        .data('autocompleteValue', key)
        .appendTo(ul);
      matchesLength++;
    }
    $input.attr('aria-expanded', true);
    // Show popup with matches, if any.
    if (this.popup) {
      if (ul.children().length) {
        $(this.popup).empty().append(ul);
        this.initMDCMenu();
        this.mdcMenu.open = true;
        $(this.ariaLive).html(Drupal.t('Autocomplete popup appears, !matches. Use the down arrow key to select a suggestion', {
          '!matches': matchesLength + ' ' + Drupal.t('suggestions')
        }));
      }
      else {
        this.hidePopup();
      }
    }
  };

  Drupal.jsAC.prototype.setStatus = function (status) {
    switch (status) {
      case 'begin':
        $(this.input).addClass('throbbing');
        $(this.ariaLive).html(Drupal.t('Searching for matches...'));
        break;
      case 'cancel':
      case 'error':
      case 'found':
        $(this.input).removeClass('throbbing');
        break;
    }
  };

  /**
   * Initializes mdc menu and set proper positioning to it.
   */
  Drupal.jsAC.prototype.initMDCMenu = function () {
    var self = this;
    var $input = $(this.input);
    var $parentDustTextfield = $input.closest('.dust-textfield');
    var $parentMdcTextfield = $input.closest('.mdc-text-field');

    this.mdcMenu = mdc.menu.MDCMenu.attachTo(this.popup);
    this.mdcMenu.setAnchorElement($parentDustTextfield[0] || this.input);

    // CornerBit.BOTTOM_START
    this.mdcMenu.setAnchorCorner(9);

    this.mdcMenu.setAnchorMargin({
      // Bottom anchor margin should be a negative value if there's something
      // other than MDC textfield inside Dust textfield wrapper.
      bottom: $parentMdcTextfield.outerHeight() - $parentDustTextfield.outerHeight(),
    });

    this.mdcMenu.listen('MDCMenu:selected', function () {
      self.hidePopup();
    });

    // Disable mdc keyboard interactions and send events to autocomplete handlers.
    $(this.mdcMenu.items).bind('keydown', function (event) {
      self.onkeydown(self.input, event);
      return false;
    });
    $(this.mdcMenu.items).bind('keyup', function (event) {
      self.onkeyup(self.input, event);
      return false;
    });
  };

  /**
   * An AutoComplete DataBase object.
   */
  Drupal.ACDB = function (uri) {
    this.uri = uri;
    this.delay = 300;
    this.cache = {};
  };

  /**
   * Performs a cached and delayed search.
   */
  Drupal.ACDB.prototype.search = function (searchString) {
    var db = this;
    this.searchString = searchString;

    // See if this string needs to be searched for anyway. The pattern ../ is
    // stripped since it may be misinterpreted by the browser.
    searchString = searchString.replace(/^\s+|\.{2,}\/|\s+$/g, '');
    // Skip empty search strings, or search strings ending with a comma, since
    // that is the separator between search terms.
    if (searchString.length <= 0 ||
      searchString.charAt(searchString.length - 1) == ',') {
      return;
    }

    // See if this key has been searched for before.
    if (this.cache[searchString]) {
      return this.owner.found(this.cache[searchString]);
    }

    // Initiate delayed search.
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(function () {
      db.owner.setStatus('begin');

      // Ajax GET request for autocompletion. We use Drupal.encodePath instead of
      // encodeURIComponent to allow autocomplete search terms to contain slashes.
      $.ajax({
        type: 'GET',
        url: Drupal.sanitizeAjaxUrl(db.uri + '/' + Drupal.encodePath(searchString)),
        dataType: 'json',
        jsonp: false,
        success: function (matches) {
          if (typeof matches.status == 'undefined' || matches.status != 0) {
            db.cache[searchString] = matches;
            // Verify if these are still the matches the user wants to see.
            if (db.searchString == searchString) {
              db.owner.found(matches);
            }
            db.owner.setStatus('found');
          }
        },
        error: function (xmlhttp) {
          Drupal.displayAjaxError(Drupal.ajaxError(xmlhttp, db.uri));
        }
      });
    }, this.delay);
  };

  /**
   * Cancels the current autocomplete request.
   */
  Drupal.ACDB.prototype.cancel = function () {
    if (this.owner) this.owner.setStatus('cancel');
    if (this.timer) clearTimeout(this.timer);
    this.searchString = '';
  };

})(jQuery);
