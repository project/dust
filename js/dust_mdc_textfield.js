/**
 * @file
 * JavaScript behaviors for the MDC text field component.
 */
(function ($, Drupal) {
  "use strict";

  /**
   * Attach MDC text field and form field scripts.
   */
  Drupal.behaviors.DustMdcTextfield = {
    attach: function (context) {
      $('.mdc-text-field', context).once('mdc-text-field', function () {
        var $this = $(this);
        var $innerInput = $this.find('.mdc-text-field__input');


        if (!$innerInput.length) {
          return;
        }

        var mdcTextfield = new Drupal.MDC.textField($this);
        var $collapsibleParent = $this.closest('fieldset.collapsed');
        // If textarea is controlled by wysiwyg we won't apply addition label
        // animations.
        if ($innerInput.is('textarea.wysiwyg')) {
          return;
        }

        // If textfield is inside collapsed fieldset, recalculate it's layout
        // when fieldset opens.
        if ($collapsibleParent.length) {
          $collapsibleParent.find('a.fieldset-title').one('click', function () {
            mdcTextfield.layout();
          });
        }

        // Recalculate text field characters count after selecting a value in
        // the autocomplete field.
        $innerInput.bind('autocompleteSelect', function () {
          mdcTextfield.foundation_.handleInput();
        });

        // Change mdc textfield layout based on input attribute modifications.
        var observer = new MutationObserver(function (mutations) {
          mutations.forEach(function (mutation) {
            if (mutation.attributeName === 'disabled') {
              if ($innerInput.is(':disabled')) {
                $this.addClass('mdc-text-field--disabled');
              }
              else {
                $this.removeClass('mdc-text-field--disabled');
              }
            }
          });
        });
        observer.observe($innerInput[0], {
          attributes: true
        });
      });
    }
  };


  /**
   * Toggle textfield disabled class via the states API.
   */
  $(document).bind('state:disabled', function (e) {
    if (e.trigger) {
      $(e.target).parents('.mdc-text-field').toggleClass('mdc-text-field--disabled', e.value);
    }
  });

  /**
   * Remove required attribute if content becomes not visible to not trigger
   * browser validation. And return back browser validation if element is visible.
   */
  $(document).bind('state:visible state:invisible', function (e) {
    var visible = (e.type === 'state:visible') ? e.value : !e.value;

    if (visible) {
      $(e.target).find('.mdc-text-field__input.required').attr('required', 'true');
    }
    else {
      $(e.target).find('.mdc-text-field__input.required').removeAttr('required');
    }
  });

})(jQuery, Drupal);
