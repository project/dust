(function ($) {
  "use strict"

  /**
   * Attach MDC menu js.
   */
  Drupal.behaviors.DustMdcMenu = {
    attach: function (context, settings) {
      $('.dust-mdc-menu', context).once('dust-mdc-menu', function () {
        var $menu = $(this),
            mdcMenu = new Drupal.MDC.menu(this);

        // Check if trigger exists.
        var $menuTrigger = $menu.parent().find('.dust-mdc-menu-trigger');
        if ($menuTrigger.length) {
          var isFixed = $menuTrigger.data('menu-anchor-position-fixed'),
              // JSON data of menu position: x, y.
              anchorAbsolute = $menuTrigger.data('menu-anchor-position-absolute') || {},
              // Possible corner variants: 'TOP_LEFT', 'BOTTOM_LEFT',
              // 'TOP_RIGHT', 'BOTTOM_RIGHT', 'TOP_START', 'BOTTOM_START',
              // 'TOP_END', 'BOTTOM_END'.
              anchorCorner = $menuTrigger.data('menu-anchor-corner') || 'BOTTOM_LEFT',
              // JSON data of menu margin: left, right, top, bottom.
              anchorMargin = $menuTrigger.data('menu-anchor-margin') || {};

          // Anchor position data should be object.
          if ($.type(anchorAbsolute) === 'string') {
            anchorAbsolute = {};
          }
          // Anchor margin data should be object.
          if ($.type(anchorMargin) === 'string') {
            anchorMargin = {};
          }

          // Set menu position around trigger element. Trigger element is
          // a new anchor now.
          mdcMenu.setFixedPosition(isFixed);
          mdcMenu.setAnchorElement($menuTrigger[0]);
          mdcMenu.setAnchorCorner(mdc.menuSurface.Corner[anchorCorner]);
          mdcMenu.setAnchorMargin(anchorMargin);
          if (!$.isEmptyObject(anchorAbsolute)) {
            mdcMenu.setAbsolutePosition(anchorAbsolute.x || 0, anchorAbsolute.y || 0);
          }

          // Set event for anchor.
          $menuTrigger.bind('click', function (e) {
            e.preventDefault();
            mdcMenu.open = !mdcMenu.open;
          });
        }
      });
    }
  }

})(jQuery);
