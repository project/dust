/**
 * @file
 * JavaScript behaviors for the MDC data table component.
 */

(function ($, Drupal) {
  "use strict";

  /**
   * Attach MDC checkbox and ripple effect.
   */
  Drupal.behaviors.DustMdcDataTable = {
    attach: function (context) {
      $('.mdc-data-table .mdc-data-table__table th:first-of-type .mdc-checkbox', context).once('mdc-data-table', function () {
        var $dataTable = $(this).parents('.mdc-data-table');

        // Add required classes for the checkbox elements and it's parent rows.
        $dataTable.find('.mdc-data-table__table thead .mdc-checkbox').addClass('mdc-data-table__header-row-checkbox').parents('th').addClass('mdc-data-table__header-cell--checkbox');
        $dataTable.find('.mdc-data-table__table tbody .mdc-checkbox').addClass('mdc-data-table__row-checkbox').each(function () {
          var $this = $(this);
          var $checkbox = $this.find('input:checkbox');

          // Add selected classes if the checkbox is checked.
          if ($checkbox.attr('checked')) {
            $(this).addClass('mdc-checkbox--selected');
            $(this).parents('tr').addClass('mdc-data-table__row--selected');
          }

          $this.parents('td').addClass('mdc-data-table__cell--checkbox');
        });


        new Drupal.MDC.dataTable($dataTable[0]);
      });
    }
  }
})(jQuery, Drupal);
