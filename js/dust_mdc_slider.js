/**
 * @file
 * Contains behaviours for MDC Slider component.
 */
(function ($, Drupal) {
  "use strict";

  /**
   * Attach MDC slider.
   */
  Drupal.behaviors.DustMdcSlider = {
    attach: function (context) {
      $('.mdc-slider', context).once('mdc-slider', function () {
        var slider = new Drupal.MDC.slider(this);
        var $form = $(this).closest('form');
        var input = $(this).data('input');

        if (typeof input !== "undefined") {
          slider.listen('MDCSlider:change', function () {
            $(document.getElementById(input)).val(slider.value);
          });
          if ($form.length) {
            $form.bind('submit', function () {
              $(document.getElementById(input)).val(slider.value);
            });
          }
        }
      });
    }
  }

})(jQuery, Drupal);
