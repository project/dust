/**
 * @file
 * JavaScript behaviors for fieldset component.
 */
(function ($, Drupal) {
  "use strict";

  /**
   * Collapsible fieldset js behaviors.
   */
  Drupal.behaviors.DustFieldset = {
    applyAccessibility: function ($legend) {
      var $fieldset = $legend.parents('fieldset');
      var expaned = $fieldset.is('.collapsed') ? 'false' : 'true';
      $legend.attr({'aria-expanded': expaned, 'role': 'button'});
    },

    attach: function (context, settings) {
      var self = this;
      $('fieldset.collapsible', context).once('DustFieldset', function () {
        // Fieldset collapsible.
        var $fieldsetLegend = $(this).find('.fieldset-legend');
        $fieldsetLegend.each(function () {
          var $this = $(this);
          if ($('.fieldset-title', $this).length === 0) {
            return;
          }
          var $button = $('a.fieldset-title', $this);
          $button.attr('aria-label', $button.text());
          $button.attr('aria-live', true);

          // Create arrow and define click arrow event as click label event.
          $this.append('<span class="arrow" />');
          $('.arrow', $this).click(function () {
            $button.click();
          });

          // Initial apply accessibility.
          self.applyAccessibility($button);

          // Add additional attributes + trigger click on space bar.
          $button.keyup(function (e) {
            if (e.keyCode === 32) {
              $(this).click();
              return false;
            }
          })
          .click(function () {
            // Wait for finishing animation.
            setTimeout(self.applyAccessibility, 1000, $(this));
          });
        });
      });
    }
  };
})(jQuery, Drupal);
