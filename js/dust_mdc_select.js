/**
 * @file
 * JavaScript behaviors for the MDC text field component.
 */
(function ($, Drupal) {
  "use strict";
  var arrayEquals = function (a, b) {
    return a.length === b.length &&
      a.every(function (val, index) { return val === b[index]; });
  };
  var SelectWrapper = /** @class */ (function () {
    function SelectWrapper($container, trailingIcons) {
      this.changeInProgress = false;
      this.optionsValues = [];
      this.$container = $container;
      this.$selectInput = $container.find('select');
      if (!this.$selectInput.length) {
        throw new Error('Missing select input.');
      }
      this.$mdcMenuSurface = $container.find('.mdc-menu-surface');
      if (!this.$mdcMenuSurface.length) {
        throw new Error('Missing MDC menu surface container.');
      }
      this.trailingIcons = trailingIcons || {};
      this.initMdcSelect();
      this.selectMutationObserver = new MutationObserver(this.initMdcSelect.bind(this));
      this.selectMutationObserver.observe(this.$selectInput[0], { childList: true });
      this.addEventListeners();
    }
    SelectWrapper.prototype.initMdcSelect = function () {
      // Destroy existing MDC select if it exists.
      this.detachMdcSelect();
      var newOptionsValues = this.getOptionsValues();
      if (!this.mdcSelect || !arrayEquals(newOptionsValues, this.optionsValues)) {
        this.initSelectOptions();
      }
      this.mdcSelect = Drupal.MDC.select(this.$container);
      // Set necessary properties if select is required.
      if (this.$selectInput.attr('required')) {
        this.mdcSelect.required = true;
      }
      // Change menu position to be on top of select if specified.
      if (this.$container.hasClass('dust-anchor-top')) {
        // @ts-ignore
        this.mdcSelect.foundation_.adapter_.setMenuAnchorCorner(0);
      }
      this.applyTrailingIcon();
    };
    SelectWrapper.prototype.applyTrailingIcon = function () {
      var value = this.mdcSelect.value === '_empty_value_' ? '' : this.mdcSelect.value;
      var $selected = this.$container.find('.mdc-list-item[data-value="' + value + '"]');
      if ($selected.length && $selected.find('> i')) {
        this.$container.find('.mdc-select__selected-text').html($selected.html());
      }
    };
    SelectWrapper.prototype.destroy = function () {
      this.detachMdcSelect();
      this.selectMutationObserver.disconnect();
    };
    SelectWrapper.prototype.detachMdcSelect = function () {
      if (this.mdcSelect) {
        this.mdcSelect.destroy();
      }
      var selectFromData = this.$container.data('MDCSelect');
      if (selectFromData) {
        this.$container.removeData('MDCSelect');
        if (selectFromData !== this.mdcSelect) {
          // Just to make sure there's no another instance initialized elsewhere.
          selectFromData.destroy();
        }
      }
    };
    SelectWrapper.prototype.initSelectOptions = function () {
      // Try saving some time if select options weren't changed.
      var newOptionsValues = this.getOptionsValues();
      if (arrayEquals(newOptionsValues, this.optionsValues)) {
        return;
      }
      this.optionsValues = newOptionsValues;
      // Remove all existing options.
      this.$mdcMenuSurface.html('');
      this.$mdcMenuSurface.append(this.generateMdcListHtml(this.$selectInput.children('option, optgroup')));
    };
    SelectWrapper.prototype.getOptionsValues = function () {
      return this.$selectInput.find('option')
        .toArray()
        .map(function (v) { return v.value; });
    };
    SelectWrapper.prototype.generateMdcListHtml = function ($optionsAndGroups) {
      var groupsExists = !!$optionsAndGroups.filter('optgroup').length;
      var optionsExists = !!$optionsAndGroups.filter('option').length;
      if (groupsExists && optionsExists) {
        throw new Error('A select element may not contain root level options and optgroups at same time.');
      }
      var $ul = $('<ul class="mdc-list"></ul>');
      groupsExists ?
        this.generateOptGroups($ul, $optionsAndGroups) :
        this.generateListOptions($ul, $optionsAndGroups);
      return $ul;
    };
    SelectWrapper.prototype.generateOptGroups = function ($container, $optGroups) {
      var self = this;
      $optGroups.each(function () {
        $container.append($('<li class="select-group-header" role="separator"></li>').text(this.label));
        self.generateListOptions($container, $(this).children('option'));
      });
    };
    SelectWrapper.prototype.generateListOptions = function ($container, $options) {
      var self = this;
      $options.each(function () {
        var $li = $('<li class="mdc-list-item"/>');
        var $span = $('<span class="mdc-list-item__text"/>');
        $span.html(this.innerHTML);
        if (this.selected) {
          $li.addClass('mdc-list-item--selected');
        }
        $li.attr('data-value', this.value === '' ? '_empty_value_' : this.value);
        if (self.trailingIcons[this.value]) {
          $li.append(self.trailingIcons[this.value]);
        }
        $li.append($span);
        $container.append($li);
      });
    };
    SelectWrapper.prototype.addEventListeners = function () {
      var _this = this;
      this.$selectInput.bind('change', function (e) {
        if (_this.changeInProgress) {
          // Prevent event handlers loop.
          return;
        }
        _this.changeInProgress = true;
        _this.mdcSelect.value = _this.$selectInput.val() === '' ? '_empty_value_' : _this.$selectInput.val();
        _this.changeInProgress = false;
      });
      this.mdcSelect.listen('MDCSelect:change', function (e) {
        if (_this.changeInProgress) {
          // Prevent event handlers loop.
          return;
        }
        _this.changeInProgress = true;
        _this.applyTrailingIcon();
        _this.$selectInput.val(e.detail.value === '_empty_value_' ? '' : e.detail.value).trigger('change');
        _this.changeInProgress = false;
      });
      // Required field has incorrect behavior, because we use '_empty_value_'
      // text for empty value. So we have to update validation of the mdc_select.
      this.$selectInput[0].addEventListener('invalid', function () {
        _this.mdcSelect.valid = false;
      });
    };
    return SelectWrapper;
  }());

  /**
   * Attach MDC text field and form field scripts.
   */
  Drupal.behaviors.DustMdcSelect = {
    attach: function (context) {
      $('.dust-mdc-select', context).once('dust-mdc-select', function () {
        var $this = $(this);
        $this.data('dust-select-wrapper',
          new SelectWrapper($this, $this.data('trailing-icons'))
        )
        $this.trigger('dustMdcSelectPrepared');
      });
    },
    detach: function (context) {
      $('.dust-mdc-select', context).each(function () {
        var $this = $(this);
        var wrapper = $this.data('dust-select-wrapper');
        if (wrapper) {
          setTimeout(function () {
            if (!document.body.contains(wrapper.$selectInput[0])) {
              // Select does not exist anymore, so destroy the SelectWrapper
              // to let garbage collector reclaim memory.
              wrapper.destroy();
              $this.removeData('dust-select-wrapper');
            }
          }, 2000);

        }
      })
    }
  }

})(jQuery, Drupal);
