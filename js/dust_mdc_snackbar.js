(function ($) {
  "use strict"

  /**
   * Attach MDC snackbar JS.
   */
  Drupal.behaviors.DustMdcSnackbar = {
    attach: function (context, settings) {
      $('.mdc-snackbar', context).once('mdc-snackbar', function () {
        new Drupal.MDC.snackbar('.mdc-snackbar')
      });
    }
  }

})(jQuery);