(function ($, Drupal) {
  'use strict';

  /**
   * Attach MDC list.
   */
  Drupal.behaviors.DustMdcList = {
    attach: function (context) {
      $('.dust-mdc-list', context).once('dust-mdc-list', function () {
        var $list = $(this);
        var $items = $list.find('> .mdc-list-item');
        var list = new Drupal.MDC.list(this);
        var isMenuList = $($items[0]).attr('role') === 'menuitem';

        // Add ripple.
        if (typeof list !== 'undefined') {
          new Drupal.MDC.ripple(this);
        }

        list.listen('MDCList:action', function (event) {
          var $innerLinks = $items.eq(event.detail.index).find('a[href]');

          // If there's only one link inside list item, follow it on item action.
          if (($innerLinks.length === 1) && !isMenuList) {
            $innerLinks[0].click();
          }
        });

        // Disable default tab behaviour for mdc lists and move to next/previous
        // list item instead.
        $items.bind('keydown', function (event) {
          var keyCode = event.which || event.keyCode;
          var count = list.foundation_.adapter_.getListItemCount();
          var index = list.foundation_.adapter_.getFocusedElementIndex();

          if (keyCode === 9) {
            if (event.shiftKey && index === 0) {
              $items.eq(index).find('*[tabindex="0"]').attr('tabindex', '-1');
            }
            else if (!event.shiftKey && index === count - 1) {
              $items.eq(index).find('*[tabindex="0"]').attr('tabindex', '-1');

              // MDC makes last item non-focusable after short delay.
              setTimeout(function () {
                $items.eq(index).attr('tabindex', '0');
              }, 10);
            }
            else {
              event.preventDefault();
              list.foundation_.adapter_.focusItemAtIndex(event.shiftKey ? index - 1 : index + 1);
            }
          }

          // Follow link inside on space and enter. Don't do that on menu event
          // because when user tries open link in new tab with ctrl + click menu
          // event also triggers, so current page navigates and new tab appears.
          var spaceEnter = [32, 13];
          var $link = $(event.target).find('a');
          if (isMenuList && (spaceEnter.indexOf(keyCode) !== -1) && $link.length) {
            $link[0].click();
          }
        });
      });
    }
  }

})(jQuery, Drupal);
