(function ($) {
  'use strict';

  if (typeof Drupal.tableDrag !== 'undefined') {

    /**
     * Basically a copy of Drupal.tableDrag method updateField. We need tweak this a bit
     * to make draggable table rows work with mdc select element.
     */
    Drupal.tableDrag.prototype.updateField = function (changedRow, group) {
      var rowSettings = this.rowSettings(group, changedRow);

      // Set the row as its own target.
      if (rowSettings.relationship == 'self' || rowSettings.relationship == 'group') {
        var sourceRow = changedRow;
      }
      // Siblings are easy, check previous and next rows.
      else if (rowSettings.relationship == 'sibling') {
        var previousRow = $(changedRow).prev('tr').get(0);
        var nextRow = $(changedRow).next('tr').get(0);
        var sourceRow = changedRow;
        if ($(previousRow).is('.draggable') && $('.' + group, previousRow).length) {
          if (this.indentEnabled) {
            if ($('.indentations', previousRow).length == $('.indentations', changedRow)) {
              sourceRow = previousRow;
            }
          }
          else {
            sourceRow = previousRow;
          }
        }
        else if ($(nextRow).is('.draggable') && $('.' + group, nextRow).length) {
          if (this.indentEnabled) {
            if ($('.indentations', nextRow).length == $('.indentations', changedRow)) {
              sourceRow = nextRow;
            }
          }
          else {
            sourceRow = nextRow;
          }
        }
      }
          // Parents, look up the tree until we find a field not in this group.
      // Go up as many parents as indentations in the changed row.
      else if (rowSettings.relationship == 'parent') {
        var previousRow = $(changedRow).prev('tr');
        while (previousRow.length && $('.indentation', previousRow).length >= this.rowObject.indents) {
          previousRow = previousRow.prev('tr');
        }
        // If we found a row.
        if (previousRow.length) {
          sourceRow = previousRow[0];
        }
            // Otherwise we went all the way to the left of the table without finding
        // a parent, meaning this item has been placed at the root level.
        else {
          // Use the first row in the table as source, because it's guaranteed to
          // be at the root level. Find the first item, then compare this row
          // against it as a sibling.
          sourceRow = $(this.table).find('tr.draggable:first').get(0);
          if (sourceRow == this.rowObject.element) {
            sourceRow = $(this.rowObject.group[this.rowObject.group.length - 1]).next('tr.draggable').get(0);
          }
          var useSibling = true;
        }
      }

      // Because we may have moved the row from one category to another,
      // take a look at our sibling and borrow its sources and targets.
      this.copyDragClasses(sourceRow, changedRow, group);
      rowSettings = this.rowSettings(group, changedRow);

      // In the case that we're looking for a parent, but the row is at the top
      // of the tree, copy our sibling's values.
      if (useSibling) {
        rowSettings.relationship = 'sibling';
        rowSettings.source = rowSettings.target;
      }

      var targetClass = '.' + rowSettings.target;
      var targetElement = $(targetClass, changedRow).get(0);

      // Check if a target element exists in this row.
      if (targetElement) {
        var sourceClass = '.' + rowSettings.source;
        var sourceElement = $(sourceClass, sourceRow).get(0);
        switch (rowSettings.action) {
          case 'depth':
            // Get the depth of the target row.
            targetElement.value = $('.indentation', $(sourceElement).closest('tr')).length;
            break;
          case 'match':
            // Update the value.
            targetElement.value = sourceElement.value;
            break;
          case 'order':
            var siblings = this.rowObject.findSiblings(rowSettings);
            // Target element should be of an MDC select.
            if ($(targetElement).is('.mdc-select')) {
              // Get a list of acceptable values.
              var values = [];
              $('li.mdc-list-item', targetElement).each(function () {
                values.push($(this).data('value'));
              });
              var maxVal = values[values.length - 1];
              // Populate the values in the siblings.
              $(targetClass, siblings).each(function () {
                // If there are more items than possible values, assign the maximum value to the row.
                if (values.length > 0) {
                  this.value = values.shift();
                }
                else {
                  this.value = maxVal;
                }
              });
            }
            else {
              // Assume a numeric input field.
              var weight = parseInt($(targetClass, siblings[0]).val(), 10) || 0;
              $(targetClass, siblings).each(function () {
                this.value = weight;
                weight++;
              });
            }
            break;
        }
      }
    };
  }

})(jQuery);