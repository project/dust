(function ($) {
  "use strict"

  /**
   * Attach MDC chips js.
   */
  Drupal.behaviors.DustMdcChips = {
    attach: function (context, settings) {
      $('.mdc-chip-set', context).once('mdc-chips', function () {
        var chipSet = new Drupal.MDC.chipSet(this);

        // If chip contains link make it not tabbable and follow it on chip
        // interaction instead.
        $.each(chipSet.chips, function () {
          var $innerLink = $(this.root_).find('a');
          if ($innerLink.length) {
            $innerLink.attr('tabindex', '-1');
            this.listen('MDCChip:interaction', function (event) {
              $innerLink[0].click();
            });
          }
        });
      });
    }
  }

  /**
   * Add focused class to the mdc chip when the trailing icon is focused.
   */
  Drupal.behaviors.DustMdcChipsTrailingIconFocusClass = {
    attach: function (context, settings) {
      $('.mdc-chip-set .mdc-chip__icon--trailing', context).once('mdc-chips-trailing-focus', function () {
        $(this).focus(function () {
          $(this).parents('.mdc-chip').addClass('focused');
        }).blur(function () {
          $(this).parents('.mdc-chip').removeClass('focused');
        })
      });
    }
  }

})(jQuery);
