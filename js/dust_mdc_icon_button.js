(function ($) {
  "use strict";

  /**
   * Attach MDC icon button js.
   */
  Drupal.behaviors.DustMdcIconButton = {
    attach: function (context, settings) {
      $('.mdc-icon-button', context).once('mdc-icon-button', function () {
        var iconButtonRipple = new Drupal.MDC.ripple(this);
        iconButtonRipple.unbounded = true;
        $(this).data('mdcRipple', iconButtonRipple);

        // Attach toggle js.
        if (typeof settings.MDCIconToggle.toggleIcon === true) {
          new Drupal.MDC.iconButtonToggle(this);
        }
      });
    }
  }

})(jQuery);
