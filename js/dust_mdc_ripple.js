(function ($) {
  "use strict"

  /**
   * Attach MDC ripple effect to specfic classes.
   */
  Drupal.behaviors.DustMDCripple = {
    attach: function (context, settings) {
      // Attach MDC ripple effect.
      function attachMdcRipple(id) {
        var rippleClass = '.' + id;
        $(rippleClass).once('mdc-ripple', function () {
          new Drupal.MDC.ripple(this);
        });
      }
      // Check for classes.
      if (typeof settings.MDCripple !== 'undefined') {
        var index;
        var classes = settings.MDCripple.classes;

        // Get unique classes.
        classes = $.grep(classes, function(v, k){
          return $.inArray(v ,classes) === k;
        });

        // Add ripple effect to given classes.
        for (index = 0; index < classes.length; ++index) {
          attachMdcRipple(classes[index]);
        }
      }
    }
  }

})(jQuery);