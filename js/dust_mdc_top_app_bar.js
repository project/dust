(function ($) {
  "use strict"

  /**
   * Attach MDC top app bar.
   */
  Drupal.behaviors.DustMdcTopAppBar = {
    attach: function (context, settings) {
      $('.mdc-top-app-bar', context).once('mdc-top-app-bar', function () {
        new Drupal.MDC.topAppBar('.mdc-top-app-bar');
      });
    }
  }

})(jQuery);