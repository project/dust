/**
 * @file
 * JavaScript behaviors for the MDC tab bar component.
 */

(function ($) {
  "use strict";

  /**
   * Attach MDC ripple effect.
   */
  Drupal.behaviors.DustMdcTabBar = {
    attach: function (context, settings) {
      $('.mdc-tab-bar', context).once('mdc-tab-bar', function () {
        var tabBar = Drupal.MDC.tabBar($(this));
        var $tabs = $(this).parents('.mdc-tabs');
        var $mdcTabs = $tabs.find('button.mdc-tab');
        // Prevent click reloading page.
        $mdcTabs.click(function (e) {
          e.preventDefault();
        });

        function activateTab(index) {
          var $tab = $tabs.find('.mdc-tab-bar--content').removeClass('active').eq(index);
          $tab.addClass('active');
          Drupal.attachBehaviors($tab);
        }

        // Add additional attributes for the tabs content elements.
        $tabs.find('.mdc-tab-bar--content').each(function (index) {
          var $this = $(this);
          $this.attr('role', 'tabpanel');

          if (typeof $mdcTabs.eq(index) !== 'undefined') {
            var $tab = $mdcTabs.eq(index);
            var label = $tab.find('.mdc-tab__text-label').text();

            if (typeof $tab.attr('id') !== 'undefined') {
              $this.attr('aria-labelledby', $tab.attr('id'));
            }
            else if (label) {
              $this.attr('aria-label', label);
            }
          }
        });

        // Add the active class for the initial active tab.
        tabBar.tabList_.forEach(function (element, index) {
          if (element.active) {
            activateTab(index)
          }
        });

        // Change the active tab content on the tab change.
        // Click on the link inside of the tab.
        tabBar.listen('MDCTabBar:activated', function (e) {
          var index = e.detail.index;
          activateTab(index);

          if ($(this).find('.mdc-tab').length > 0) {
            if ($(this).find('.mdc-tab').eq(index).find('a').length) {
              $(this).find('.mdc-tab').eq(index).find('a').click();
            }
          }
        })
      });
    }
  }

})(jQuery);
