(function ($, Drupal) {
  "use strict"

  /**
   * Attach MDC checkbox and ripple effect.
   */
  Drupal.behaviors.DustMdcCheckbox = {
    attach: function (context) {
      $('.mdc-checkbox', context).once('mdc-checkbox', function () {
        if (!$(this).find('.mdc-checkbox__native-control').length) {
          return;
        }
        var checkbox = new Drupal.MDC.checkbox(this);
        var formfield = new Drupal.MDC.formField($(this).closest('.mdc-form-field')[0]);

        formfield.input = checkbox;
      });

      // Set aria label for table checkbox that selects/deselects all.
      $('.mdc-data-table__header-row-checkbox', context).once('mdc-table-checkbox', function () {
        var strings = {
          selectAll: Drupal.t('Select all rows in this table'),
          selectNone: Drupal.t('Deselect all rows in this table')
        };
        var $selectAll = $(this).find('.mdc-checkbox__native-control');
        var $allCheckboxes = $(this).closest('.mdc-data-table').find('.mdc-checkbox__native-control');

        // Set default aria-label on page load.
        if (!$selectAll.is(':checked') || $selectAll.attr('checked') === 'mixed') {
          $selectAll.attr('aria-label', strings.selectAll);
        }
        else {
          $selectAll.attr('aria-label', strings.selectNone);
        }

        // Change all checkbox label when checkboxes state changes.
        $allCheckboxes.bind('change', function () {
          setTimeout(function () {
            if (!$selectAll.is(':checked') || $selectAll.attr('checked') === 'mixed') {
              $selectAll.attr('aria-label', strings.selectAll);
            }
            else {
              $selectAll.attr('aria-label', strings.selectNone);
            }
          }, 0);
        });
      });
    }
  }
})(jQuery, Drupal);
