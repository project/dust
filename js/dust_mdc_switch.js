(function ($) {
  "use strict"

  /**
   * Attach MDC top app bar.
   */
  Drupal.behaviors.DustMdcSwitch = {
    attach: function (context, settings) {
      $('.mdc-switch', context).once('mdc-switch', function () {
        new Drupal.MDC.switch($(this));
      });
    }
  }

})(jQuery);
