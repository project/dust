/**
 * @file
 * JavaScript behaviors for the MDC line ripple component.
 */
(function ($, Drupal) {
  "use strict";

  /**
   * Attach MDC checkbox and ripple effect.
   */
  Drupal.behaviors.DustMdcLineRipple = {
    attach: function (context) {
      $('.mdc-line-ripple', context).once('mdc-line-ripple', function () {
        new Drupal.MDC.lineRipple(this);
      });
    }
  }
})(jQuery, Drupal);
