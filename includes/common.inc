<?php

/**
 * @file
 * Common dust theme function.
 */

/**
 * Discovers and registers preprocess, process theme functions.
 *
 * @param string $theme
 *   The drupal theme name.
 * @param array $registry
 *   Theme registry array.
 */
function dust_discover_hooks($theme, array &$registry) {
  foreach (array('process', 'preprocess') as $type) {
    // Iterate over all preprocess/process files in the current theme.
    $discovered_files = dust_discover_files($theme, $type);
    foreach ($discovered_files as $item) {
      $callback = "{$theme}_{$type}_{$item->hook}";

      // If there is no hook with that name, continue.
      if (!array_key_exists($item->hook, $registry)) {
        continue;
      }

      // Append the included (pre-)process hook to the array of functions.
      $registry[$item->hook]["$type functions"][] = $callback;

      // By adding this file to the 'includes' array we make sure that it is
      // available when the hook is executed.
      $registry[$item->hook]['includes'][] = $item->uri;
    }
  }
}

/**
 * Scans for files of a certain type in the current theme's path.
 *
 * @param string $theme
 *   The drupal theme name.
 * @param string $type
 *   Theme registry array.
 *
 * @return array
 *   An associative array (keyed on the chosen key) of objects
 *   with 'uri', 'filename' and 'name' members
 *   corresponding to the matching files.
 */
function dust_discover_files($theme, $type) {
  $length = -(strlen($type) + 1);

  $path = drupal_get_path('theme', $theme);
  $mask = '/.' . $type . '.inc$/';

  // Recursively scan the folder for the current step for (pre-)process
  // files and write them to the registry.
  $files = file_scan_directory($path . '/' . $type, $mask);
  foreach ($files as &$file) {
    $file->hook = strtr(substr($file->name, 0, $length), '-', '_');
  }

  return $files;
}

/**
 * Retrieves specific info theme .info file.
 *
 * Similar to how other drupal themes do it,
 * IE Omega & Bootstrap as opposed to just do a css_alter.
 *
 * @param string $theme_key
 *   The Drupal theme machine.
 * @param string $key
 *   The key name to retrieve from .info file.
 * @param bool $base_themes
 *   Search base theme info as well.
 *
 * @return string|array|false
 *   Returns differently depending on data.
 */
function dust_get_theme_info($theme_key = NULL, $key = NULL, $base_themes = TRUE) {
  // If no $theme_key is given, use the current theme if we can determine it.
  if (!isset($theme_key)) {
    $theme_key = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : FALSE;
  }
  if ($theme_key) {
    $themes = list_themes();
    if (!empty($themes[$theme_key])) {
      $theme = $themes[$theme_key];
      // If a key name was specified, return just that array.
      if ($key) {
        $value = FALSE;
        // Recursively add base theme values.
        if ($base_themes && isset($theme->base_themes)) {
          foreach (array_keys($theme->base_themes) as $base_theme) {
            $value = dust_get_theme_info($base_theme, $key);
          }
        }

        if (!empty($themes[$theme_key])) {
          $info = $themes[$theme_key]->info;
          // Allow array traversal.
          $keys = explode('][', $key);
          foreach ($keys as $parent) {
            if (isset($info[$parent])) {
              $info = $info[$parent];
            }
            else {
              $info = FALSE;
            }
          }
          if (is_array($value)) {
            if (!empty($info)) {
              if (!is_array($info)) {
                $info = array($info);
              }
              $value = drupal_array_merge_deep($value, $info);
            }
          }
          else {
            if (!empty($info)) {
              if (empty($value)) {
                $value = $info;
              }
              else {
                if (!is_array($value)) {
                  $value = array($value);
                }
                if (!is_array($info)) {
                  $info = array($info);
                }
                $value = drupal_array_merge_deep($value, $info);
              }
            }
          }
        }
        return $value;
      }
      // If no info $key was specified, just return the entire info array.
      return $theme->info;
    }
  }
  return FALSE;
}
