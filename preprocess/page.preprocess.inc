<?php

/**
 * @file
 * Preprocess functions for page.
 */

/**
 * Implements hook_preprocess_page().
 */
function dust_preprocess_page(&$variables) {
  if (!empty($variables['page']['sidebar_first'])) {
    $left = $variables['page']['sidebar_first'];
  }

  if (!empty($variables['page']['sidebar_second'])) {
    $right = $variables['page']['sidebar_second'];
  }
  // Dynamic mdc sidebars.
  if (!empty($left) && !empty($right)) {
    $variables['main_grid'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone mdc-layout-grid__cell--span-6-tablet mdc-layout-grid__cell--span-4-phone';
    $variables['sidebar_left'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-4-phone';
    $variables['sidebar_right'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-4-phone';
  }
  elseif (empty($left) && !empty($right)) {
    $variables['main_grid'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone mdc-layout-grid__cell--span-9-tablet mdc-layout-grid__cell--span-4-phone';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-4-phone';
  }
  elseif (!empty($left) && empty($right)) {
    $variables['main_grid'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-9-tablet mdc-layout-grid__cell--span-4-phone';
    $variables['sidebar_left'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-4-phone';
    $variables['sidebar_right'] = '';
  }
  else {
    $variables['main_grid'] = 'mdc-layout-grid__cell mdc-layout-grid__cell--span-12-phone';
    $variables['sidebar_left'] = '';
    $variables['sidebar_right'] = '';
  }
}
