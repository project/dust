<?php

/**
 * @file
 * Override or insert variables into the HTML templates.
 *
 * To add dynamically your own classes use
 * $vars['classes_array'][] = 'my_class';
 */

/**
 * Implements hook_preprocess_html().
 */
function dust_preprocess_html(&$vars) {

  // Add material web components library.
  if ($mdc_path = libraries_get_path('material-components-web')) {
    // Add css.
    drupal_add_css($mdc_path . '/dist/material-components-web.min.css', [
      'group' => CSS_THEME,
      'every_page' => TRUE,
      'weight' => '-1',
    ]);
    // Add js.
    drupal_add_js($mdc_path . '/dist/material-components-web.min.js',
      [
        'group' => JS_THEME,
        'every_page' => TRUE,
        'weight' => '-1',
      ]
    );
  }

  // Add Material Icon set.
  if ($mdc_icons_path = libraries_get_path('material-design-icons')) {
    drupal_add_css($mdc_icons_path . '/iconfont/material-icons.css');
  }

  // Add default roboto font library.
  // Roboto is placed in fonts sub dir of libraries.
  if (!$roboto_font_path = libraries_get_path('fonts')) {
    watchdog('log', 'Please create fonts directory at /sites/all/libraries/', [], WATCHDOG_ERROR);
  }
  if ($roboto_font_path) {
    drupal_add_css($roboto_font_path . '/roboto/roboto.css');
  }

  // Add meta.
  $meta = [
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no',
    ],
  ];
  drupal_add_html_head($meta, 'viewport');

  // Add mdc typography class.
  $vars['attributes']['class'][] = 'mdc-typography';
}
