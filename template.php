<?php

/**
 * @file
 * Template.php file for dust base theme.
 */

/**
 * Include dust common functions.
 */
include_once dirname(__FILE__) . '/includes/common.inc';

/**
 * Implements hook_css_alter().
 */
function dust_css_alter(&$css) {
  // Exclude CSS files from theme defined in .info file.
  if ($excludes = dust_get_theme_info(NULL, 'exclude][css')) {
    $excludes = array_merge($excludes, str_replace('.css', '-rtl.css', $excludes));
    $css = array_diff_key($css, drupal_map_assoc($excludes));
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function dust_theme_registry_alter(&$registry) {
  global $theme_key;
  $available_themes = list_themes();
  $base_themes = array();
  if (isset($available_themes[$theme_key]->info['base theme'])) {
    $base_themes = drupal_find_base_themes($available_themes, $theme_key);
  }
  // Use materialize theme as it provides necessary
  // functionality for page elements.
  $base_themes += array('dust' => 'dust');
  $base_themes += array($theme_key => $theme_key);

  foreach ($base_themes as $key => $theme_human_name) {
    if (empty($theme_key)) {
      continue;
    }
    dust_discover_hooks($key, $registry);
  }

  // Add attributes variable for mdc-table element.
  $registry['table']['variables']['wrapper_attributes'] = array();
}

/**
 * Implements hook_theme().
 *
 * Declared theme functions for mdc core dust components.
 */
function dust_theme($existing, $type, $theme, $path) {
  return array(
    'mdc_linear_progress_indicator' => array(
      'variables' => array(
        'percent' => NULL,
        'label' => NULL,
        'attributes' => array(),
      ),
      'template'  => 'mdc-linear-progress-indicator',
      'path' => $path . '/templates/components/mdc_linear_progress_indicator',
      'preprocess functions' => array(
        'template_preprocess_mdc_linear_progress_indicator',
      ),
    ),
    'mdc_linear_progress_indicator_buffer' => array(
      'variables' => array(
        'percent' => NULL,
        'label' => NULL,
        'attributes' => array(),
      ),
      'template'  => 'mdc-linear-progress-indicator-buffer',
      'path' => $path . '/templates/components/mdc_linear_progress_indicator',
      'preprocess functions' => array(
        'template_preprocess_mdc_linear_progress_indicator',
      ),
    ),
    'mdc_linear_progress_indicator_indeterminate' => array(
      'variables' => array(
        'label' => NULL,
        'attributes' => array(),
      ),
      'template'  => 'mdc-linear-progress-indicator-indeterminate',
      'path' => $path . '/templates/components/mdc_linear_progress_indicator',
      'preprocess functions' => array(
        'template_preprocess_mdc_linear_progress_indicator',
      ),
    ),
    'mdc_fab' => array(
      'variables' => array(
        'icon' => NULL,
        'label' => NULL,
        'attributes' => NULL,
        'type' => NULL,
        'trailing_icon' => FALSE,
      ),
      'template'  => 'mdc-fab',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_fab',
      ),
    ),
    'mdc_button' => array(
      'variables' => array(
        'label' => NULL,
        'attributes' => NULL,
        'icon' => NULL,
        'type' => 'raised',
      ),
      'template'  => 'mdc-button',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_button',
      ),
    ),
    'mdc_card' => array(
      'variables' => array(
        'image' => NULL,
        'title' => NULL,
        'subtitle' => NULL,
        'body' => NULL,
        'action_buttons' => NULL,
        'action_icons' => NULL,
        'attributes' => array(),
      ),
      'template'  => 'mdc-card',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_card',
      ),
    ),
    'mdc_checkbox' => array(
      'variables' => array(
        'attributes' => NULL,
      ),
      'template'  => 'mdc-checkbox',
      'path' => $path . '/templates/components',
      'preprocess functions' => [
        'template_preprocess_mdc_checkbox',
      ],
    ),
    'mdc_radio' => array(
      'variables' => array(
        'attributes' => NULL,
      ),
      'template'  => 'mdc-radio',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_radio',
      ),
    ),
    'mdc_drawer' => array(
      'variables' => array(
        'type' => NULL,
        'title' => NULL,
        'title_tag' => 'h3',
        'subtitle' => NULL,
        'content' => NULL,
        'direction' => NULL,
        'content_direction' => NULL,
      ),
      'template' => 'mdc-drawer',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_drawer',
      ),
    ),
    'mdc_top_app_bar' => array(
      'variables' => array(
        'attributes' => NULL,
        'top_app_start' => NULL,
        'top_app_end' => NULL,
        'fixed' => TRUE,
        'fixed_adjust' => FALSE,
        'top_app_content' => NULL,
      ),
      'template'  => 'mdc-top-app-bar',
      'path' => $path . '/templates/components/',
      'preprocess functions' => array(
        'template_preprocess_mdc_top_app_bar',
      ),
    ),
    'mdc_icon_button' => array(
      'variables' => array(
        'icon' => NULL,
        'toggle_icon' => NULL,
        'attributes' => array(),
        'aria_label' => NULL,
      ),
      'template'  => 'mdc-icon-button',
      'path' => $path . '/templates/components/',
      'preprocess functions' => array(
        'template_preprocess_mdc_icon_button',
      ),
    ),
    'mdc_chips' => array(
      'variables' => array(
        'items' => NULL,
        'attributes' => array(),
        'type' => NULL,
      ),
      'template'  => 'mdc-chip',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_chips',
      ),
    ),
    'switch' => array(
      'render element' => 'element',
    ),
    'mdc_switch' => array(
      'variables' => array(
        'attributes' => NULL,
      ),
      'template'  => 'mdc-switch',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_switch',
      ),
    ),
    'mdc_menu' => array(
      'variables' => array(
        'links' => array(),
        'menu_trigger' => NULL,
        'menu_attributes' => array(),
        'list_attributes' => array(),
      ),
      'template'  => 'mdc-menu',
      'path' => $path . '/templates/components/mdc_menu',
      'preprocess functions' => array(
        'template_preprocess_mdc_menu',
      ),
    ),
    'mdc_menu_tree_wrapper' => array(
      'render element' => 'tree',
    ),
    'mdc_menu_block_wrapper' => array(
      'variables' => array(
        'tree' => NULL,
        'menu_trigger' => NULL,
      ),
      'template'  => 'mdc-menu-block--wrapper',
      'path' => $path . '/templates/components/mdc_menu',
    ),
    'mdc_list' => array(
      'variables' => array(
        'items' => NULL,
        'type' => 'ul',
        'divider' => FALSE,
        'attributes' => NULL,
        'icon_lead' => NULL,
        'icon_trail' => NULL,
      ),
      'template'  => 'mdc-list',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_list',
      ),
    ),
    'mdc_snackbar' => array(
      'variables' => array(
        'icon' => NULL,
        'type' => NULL,
        'message' => NULL,
        'actions' => NULL,
        'attributes' => array(),
      ),
      'template'  => 'mdc-snackbar',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_snackbar',
      ),
    ),
    'mdc_tab_bar' => array(
      'variables' => array(
        'attributes' => array(),
        'stacked' => FALSE,
        'tabs' => array(),
      ),
      'template' => 'mdc-tab-bar',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_tab_bar',
      ),
    ),
    'mdc_slider' => array(
      'render element' => 'element',
      'template'  => 'mdc-slider',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_slider',
      ),
    ),
    'mdc_image_list' => array(
      'variables' => array(
        'items' => NULL,
        'attributes' => NULL,
        'mansonry_layout' => FALSE,
      ),
      'template'  => 'mdc-image-list',
      'path' => $path . '/templates/components/',
      'preprocess functions' => array(
        'template_preprocess_mdc_image_list',
      ),
    ),
    'mdc_dialog' => array(
      'variables' => array(
        'title' => NULL,
        'content' => NULL,
        'attributes' => NULL,
        'actions' => NULL,
      ),
      'template'  => 'mdc-dialog',
      'path' => $path . '/templates/components/',
      'preprocess functions' => array(
        'template_preprocess_mdc_dialog',
      ),
    ),
    'mdc_textfield' => array(
      'render element' => 'element',
      'template' => 'mdc-textfield',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_textfield',
        'template_preprocess_mdc_textfields',
      ),
    ),
    'mdc_password' => array(
      'render element' => 'element',
      'template' => 'mdc-textfield',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_password',
        'template_preprocess_mdc_textfields',
      ),
    ),
    'mdc_textarea' => array(
      'render element' => 'element',
      'template' => 'mdc-textarea',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_textarea',
        'template_preprocess_mdc_textfields',
      ),
    ),
    'mdc_date' => array(
      'render element' => 'element',
      'template' => 'mdc-textfield',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_datetime',
        'template_preprocess_mdc_textfields',
      ),
    ),
    'mdc_time' => array(
      'render element' => 'element',
      'template' => 'mdc-textfield',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_datetime',
        'template_preprocess_mdc_textfields',
      ),
    ),
    'mdc_select' => array(
      'render element' => 'element',
      'template' => 'mdc-select',
      'path' => $path . '/templates/components',
      'preprocess functions' => array(
        'template_preprocess_mdc_select',
      ),
    ),
  );
}

/**
 * Implements hook_element_info_alter().
 */
function dust_element_info_alter(&$type) {
  $type['textfield']['#theme'] = 'mdc_textfield';
  $type['machine_name']['#theme'] = 'mdc_textfield';
  $type['password']['#theme'] = 'mdc_password';
  $type['textarea']['#theme'] = 'mdc_textarea';

  $index = array_search('form_process_select', $type['select']['#process']);
  $type['select']['#theme'] = 'mdc_select';
  $type['select']['#process'][$index] = 'dust_form_process_select';

  $index = array_search('form_process_fieldset', $type['fieldset']['#process']);
  $type['fieldset']['#process'][$index] = 'dust_form_process_fieldset';

  $type['date']['#theme'] = 'mdc_date';
  $index = array_search('form_process_date', $type['date']['#process']);
  $type['date']['#process'][$index] = 'dust_form_process_date';
  $type['date']['#value_callback'] = 'dust_form_date_value_callback';

  if (module_exists('dust_form_elements') && isset($type['time'])) {
    $type['time']['#theme'] = 'mdc_time';
  }

  if (isset($type['rangefield'])) {
    $type['rangefield']['#theme'] = 'mdc_slider';
    $type['rangefield']['#element_validate'][] = 'dust_rangefield_element_validate';
  }

  if (module_exists('webform')) {
    $type['webform_email']['#theme'] = 'mdc_textfield';
    $type['webform_email']['#process'][] = 'dust_webform_email_process';
  }
}

/**
 * Implements hook_js_alter().
 */
function dust_js_alter(&$javascript) {
  $media_js = drupal_get_path('module', 'media') . '/js/media.js';

  // Replace default media.js file with ours.
  if (isset($javascript[$media_js])) {
    $javascript[$media_js]['data'] = drupal_get_path('theme', 'dust') . '/js/dust_media.js';
  }

  // Replace core autocomplete to add mdc menu to autocomplete.
  if (isset($javascript['misc/autocomplete.js'])) {
    $javascript['misc/autocomplete.js']['data'] = drupal_get_path('theme', 'dust') . '/js/dust_autocomplete.js';
    $javascript['misc/autocomplete.js']['version'] = NULL;
  }
}

/**
 * Replacement of form_process_select().
 *
 * @param array $element
 *   Select form element.
 *
 * @return array
 *   THe select form element
 */
function dust_form_process_select(array $element, &$form_state, $form) {
  // #multiple select fields need a special #name.
  if ($element['#multiple']) {
    $element['#attributes']['multiple'] = 'multiple';
    $element['#attributes']['name'] = $element['#name'] . '[]';
  }
  // A non-#multiple select needs special handling to prevent user agents from
  // preselecting the first option without intention. #multiple select lists do
  // not get an empty option, as it would not make sense, user interface-wise.
  else {
    $required = $element['#required'];
    // If the element is required and there is no #default_value, then add an
    // empty option that will fail validation, so that the user is required to
    // make a choice. Also, if there's a value for #empty_value or
    // #empty_option, then add an option that represents emptiness.
    if (($required && !isset($element['#default_value'])) || isset($element['#empty_value']) || isset($element['#empty_option'])) {
      $element += array(
        '#empty_value' => NULL,
        '#empty_option' => '',
      );
      // The empty option is prepended to #options and purposively not merged
      // to prevent another option in #options mistakenly using the same value
      // as #empty_value.
      $empty_option = array($element['#empty_value'] => $element['#empty_option']);
      $element['#options'] = $empty_option + $element['#options'];
    }
  }
  // Assign input to form state.
  if (!empty($form_state['input'][$element['#name']])) {
    $form_state['values'][$element['#name']] = $form_state['input'][$element['#name']];
  }
  // Remove the default form select label.
  // @see mdc_select theming.
  $element['#title_display'] = 'none';

  return $element;
}

/**
 * Preprocess MDC chips template.
 *
 * @param array $vars
 *   An associative array containing:
 *   - items: Chip items including 'leading_icon', 'text & 'trailing_icon.
 *   - attributes: Any additional attributes to add.
 *   - type: Type of chip set to display either 'filter' or 'choice'.
 *
 * @see mdc-chips.tpl.php
 */
function template_preprocess_mdc_chips(array &$vars) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_chips.js');

  $type = $vars['type'];
  $attributes = &$vars['attributes'];

  // Default attributes.
  $attributes['class'][] = 'mdc-chip-set';

  // Add class for type given.
  if (isset($type) && is_string($type)) {
    switch ($type) {
      case 'filter':
        $attributes['class'][] = 'mdc-chip-set--filter';
        break;

      case 'choice':
        $attributes['class'][] = 'mdc-chip-set--choice';
        break;
    }
  }
}

/**
 * Preprocess MDC top app bar theme hook.
 *
 * @see mdc-top-app-bar.tpl.php
 */
function template_preprocess_mdc_top_app_bar(&$vars) {
  // Attach js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_top_app_bar.js');

  $attributes = &$vars['attributes'];
  $attributes['class'][] = 'mdc-top-app-bar';
  $attributes['role'][] = 'banner';

  // Add fixed toolbar class.
  if ($vars['fixed']) {
    $attributes['class'][] = 'mdc-top-app-bar--fixed';
  }
}

/**
 * Preprocess MDC menu template.
 *
 * @see mdc-menu.tpl.php
 */
function template_preprocess_mdc_menu(&$vars) {
  // Menu attributes.
  $menu_attributes = &$vars['menu_attributes'];
  $menu_attributes['class'][] = 'mdc-menu';
  $menu_attributes['class'][] = 'dust-mdc-menu';
  $menu_attributes['class'][] = 'mdc-menu-surface';

  // Unordered list attributes.
  $list_attributes = &$vars['list_attributes'];
  $list_attributes['class'][] = 'mdc-list';
  $list_attributes['role'] = 'menu';

  // Attach js mainly for accessibility.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_menu.js');

  foreach ($vars['links'] as $i => &$link) {
    $link_attributes = $link['link_attributes'] ?? [];
    $attributes = $link['attributes'] ?? [];

    $attributes['role'] = 'menuitem';
    $attributes['class'][] = 'mdc-list-item';
    if ($i === 0) {
      $attributes['tabindex'] = 0;
    }
    $link_attributes['href'] = '#';
    if (isset($link['path'])) {
      $options = $link['options'] ?? [];
      $link_attributes['href'] = url($link['path'], $options);
    }

    $link['link_attributes'] = $link_attributes;
    $link['attributes'] = $attributes;
  }

  if (!empty($vars['menu_trigger']) && is_array($vars['menu_trigger']) && isset($vars['menu_trigger']['#theme'])) {
    $menu_trigger = $vars['menu_trigger'];
    $menu_trigger['#attributes']['class'][] = 'dust-mdc-menu-trigger';
    $vars['menu_trigger'] = drupal_render($menu_trigger);
  }
}

/**
 * Preprocess MDC linear progress indicators theme hook.
 *
 * @see mdc-dialog.tpl.php
 */
function template_preprocess_mdc_dialog(&$vars) {
  $attributes = &$vars['attributes'];
  $attributes['class'][] = 'mdc-dialog';
  $attributes['role'][] = 'alertdialog';
  $attributes['aria-modal'][] = 'true';
  $vars['id'] = $id = drupal_html_id('mdc-dialog');

  if (!empty($vars['title'])) {
    $attributes['aria-labelledby'] = $id . '-title';
  }
  if (!empty($vars['content'])) {
    $attributes['aria-describedby'] = $id . '-content';
  }
}

/**
 * Preprocess MDC linear progress indicators theme hook.
 *
 * Inputs: $percent.
 *
 * @see mdc-linear-progress-indicator.tpl.php
 * @see mdc-linear-progress-indicator-buffer.tpl.php
 * @see mdc-linear-progress-indicator-indeterminate.tpl.php
 */
function template_preprocess_mdc_linear_progress_indicator(&$vars) {
  // Add scale variable for transform.
  if (!empty($vars['percent'])) {
    $vars['scale'] = intval($vars['percent']) / 100;
  }

  // Label if any.
  if (!empty($vars['label'])) {
    $vars['label'] = check_plain($vars['label']);
  }

  $attributes = &$vars['attributes'];
  $attributes['class'][] = 'dust-mdc-linear-progress-indicator';
  $attributes['class'][] = 'mdc-linear-progress-indicator';
}

/**
 * Override theme progress bar to use MDC html linear progress bar.
 *
 * @see theme_progress_bar()
 */
function dust_progress_bar($variables) {
  return theme('mdc_linear_progress_indicator', ['percent' => $variables['percent']]);
}

/**
 * Preprocess MDC card theme hook.
 *
 * @see mdc-card.tpl.php
 */
function template_preprocess_mdc_card(&$vars) {
  if (!empty($vars['image'])) {
    $image = &$vars['image'];
    // Image url.
    $vars['image_url'] = file_create_url($image->uri) ?? '';
    // Image title.
    $vars['image_title'] = $image->title ?? '';
  }

  // Attach ripple js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_ripple.js');
  drupal_add_js([
    'MDCripple' => [
      'classes' => ['mdc-card__primary-action'],
    ],
  ], 'setting');

  $vars['attributes']['class'][] = 'mdc-card';
}

/**
 * Preprocess MDC radio template.
 *
 * @see mdc-radio.tpl.php
 */
function template_preprocess_mdc_radio(&$vars) {
  // Attach js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_radio.js');
  // Default attributes.
  $attributes = &$vars['attributes'];
  $attributes['role'][] = 'radio';
}

/**
 * Preprocess MDC drawer template.
 *
 * @see mdc-drawer.tpl.php
 */
function template_preprocess_mdc_drawer(&$vars) {
  // Default attributes.
  $attributes = &$vars['attributes'];
  $attributes['class'][] = 'mdc-drawer';

  $type = $vars['type'];

  // Additional based on drawer type attributes.
  $type_dependent_classes = [
    'modal' => 'mdc-drawer--modal',
    'dismissible' => 'mdc-drawer--dismissible',
  ];
  if (isset($type_dependent_classes[$type])) {
    $attributes['class'][] = $type_dependent_classes[$type];
  }

  // Drawer direction.
  if (isset($vars['direction'])) {
    $direction = $vars['direction'];
    switch ($direction) {
      case 'right':
        $attributes['dir'] = 'rtl';
        break;

      case 'left':
        $attributes['dir'] = 'ltr';
        break;
    }
  }

  // Drawer content direction.
  if (isset($vars['content_direction'])) {
    $content_direction = &$vars['content_direction'];
    switch ($content_direction) {
      case 'right':
        $content_direction = 'rtl';
        break;

      case 'left':
        $content_direction = 'ltr';
        break;
    }
  }
}

/**
 * Preprocess MDC checkbox template.
 *
 * @see mdc-checkbox.tpl.php
 */
function template_preprocess_mdc_checkbox(&$vars) {
  // Attach js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_checkbox.js');
  // Default attributes.
  $attributes = &$vars['attributes'];
  $attributes['aria-checked'][] = 'mixed';
}

/**
 * Preprocess MDC button theme hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - label: Button label
 *   - icon: The icon to display.
 *   - type: Is one of 'raised', 'outlined' or 'unelevated'.
 *   - attributes: Any additional attributes to add.
 *
 * @see mdc-button.tpl.php
 */
function template_preprocess_mdc_button(array &$vars) {
  // Attach ripple js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_ripple.js');
  drupal_add_js(
    array(
      'MDCripple' => array(
        'classes' => array('mdc-button'),
      ),
    ), 'setting');
  $type = $vars['button_type'] ?? $vars['type'];

  // Default attributes.
  $attributes = array(
    'class' => array(
      'mdc-button',
      'mdc-ripple-surface',
    ),
    'role' => 'button',
  );

  // If not empty additional attributes.
  $attributes = !empty($vars['attributes'])
    // Merge any additional attributes.
    ? drupal_array_merge_deep($attributes, $vars['attributes'])
    // Return existing ones.
    : $attributes;

  // Add class for type given.
  if (isset($type) && is_string($type)) {
    switch ($type) {
      case 'button':
      case 'simple':
        break;

      case 'raised':
      case 'submit':
        $attributes['class'][] = 'mdc-button--raised';
        break;

      case 'outlined':
        $attributes['class'][] = 'mdc-button--outlined';
        break;

      case 'unelevated':
        $attributes['class'][] = 'mdc-button--unelevated';
        break;
    }

    if (!isset($attributes['type'])) {
      $attributes['type'] = $type;
    }
  }

  $vars['attributes'] = $attributes;
}

/**
 * Preprocess MDC icon button.
 *
 * @param array $vars
 *   An associative array containing:
 *   - attributes: Additional mdc icon button attributes.
 *   - icon: The mdc icon to use for example 'Favourite.
 *   - toggle_icon: The mdc toggle icon to use if any for example 'Favourite.
 *   - aria_label: The aria label attribute.
 *
 * @see mdc-icon-button.tpl.php
 */
function template_preprocess_mdc_icon_button(array &$vars) {
  // Add js MDC icon js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_icon_button.js');

  // Default attributes.
  $vars['attributes']['class'][] = 'mdc-icon-button';
  $vars['attributes']['type'] = $vars['attributes']['type'] ?? 'button';

  // Add aria label.
  if (!empty($vars['aria_label']) && !isset($vars['attributes']['aria-label'])) {
    $vars['attributes']['aria-label'] = $vars['aria_label'];
  }

  // Add toggle JS.
  drupal_add_js([
    'MDCIconToggle' => [
      'toggleIcon' => !empty($vars['toggle_icon']),
    ],
  ], 'setting');
}

/**
 * Preprocess MDC floating action button theme hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - label: fab label
 *   - icon: The icon to display.
 *   - type: Is one of 'raised', 'outlined' or 'unelevated'.
 *   - attributes: Any additional attributes to add.
 *   - trailing_icon: True to trail icon.
 *
 * @throws \Exception
 *
 * @see mdc-fab.tpl.php
 */
function template_preprocess_mdc_fab(array &$vars) {
  $type = $vars['type'];
  // Fab icon.
  $vars['leading'] = $icon = '<i class="material-icons mdc-fab__icon" aria-hidden="true">' . $vars['icon'] . '</i>';

  // Fab label.
  $vars['trailing'] = $label = isset($vars['label'])
    ? theme('html_tag', array(
      'element' => array(
        '#tag' => 'span',
        '#value' => check_plain($vars['label']),
        '#attributes' => array(
          'class' => ['mdc-fab__label'],
        ),
      ),
    ))
    : '';

  // Attach ripple js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_ripple.js');
  drupal_add_js(
    array(
      'MDCripple' => array(
        'classes' => array('mdc-fab'),
      ),
    ), 'setting');

  // Default attributes.
  $attributes = $vars['attributes'];
  $attributes['class'][] = 'mdc-fab';

  // Add class for type given.
  if (isset($type) && is_string($type)) {
    switch ($type) {
      case 'mini':
        $attributes['class'][] = 'mdc-fab--mini';
        break;

      case 'extended':
        if (!empty($vars['label'])) {
          $attributes['class'][] = 'mdc-fab--extended';
          // Trailing icon.
          if ($vars['trailing_icon']) {
            // Switch variables.
            [$vars['leading'], $vars['trailing']] = [$label, $icon];
          }
        }
        break;
    }
  }

  $vars['attributes'] = $attributes;
}

/**
 * Returns MDC html for a form element.
 *
 * @see theme_form_element()
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-form-field
 */
function dust_form_element($variables) {
  $element =& $variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }

  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array(
    'form-item',
  );

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
      ' ' => '-',
      '_' => '-',
      '[' => '-',
      ']' => '',
    ));
  }

  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  if (!_dust_form_field_is_textfield($element['#type'])) {
    $output .= '<div class="mdc-form-field">';
  }

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';
  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }
  if (!_dust_form_field_is_textfield($element['#type'])) {
    $output .= '</div>';
  }
  // Description in textfields is handled within its theming.
  if (!empty($element['#description']) && !_dust_form_field_is_textfield($element['#type'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }
  $output .= "</div>\n";
  return $output;
}

/**
 * Returns HTML for a form element label and required marker.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #required, #title, #id, #value, #description,
 *     #label_for.
 *
 * @return string
 *   An HTML string containing a form label..
 */
function dust_form_element_label(array $variables) {
  $element = $variables['element'];
  $element_type = isset($element['#type']) ? $element['#type'] : '';
  $attributes = array();

  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // Label for textfields is handled within its theming.
  if (_dust_form_field_is_textfield($element_type)) {
    return '';
  }
  elseif ($element_type === 'select') {
    $attributes['class'] = 'element-invisible';
  }

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible' || (isset($element['#type']) && in_array($element['#type'], array('checkboxes', 'radios')))) {
    $attributes['class'] = 'element-invisible';
  }

  // Range field in MDC is not handled by input type range,
  // so we use aria-labelledby on slider component instead.
  if (isset($element['#type']) && $element['#type'] === 'rangefield') {
    $attributes['id'] = $element['#id'] . '-label';
  }
  // Use the element's ID as the default value of the "for" attribute (to
  // associate the label with this form element), but allow this to be
  // overridden in order to associate the label with a different form element
  // instead.
  elseif (!empty($element['#label_for'])) {
    $attributes['for'] = $element['#label_for'];
  }
  elseif (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // Set id for a label.
  if (!empty($element['#label_id'])) {
    $attributes['id'] = $element['#label_id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

/**
 * Returns MDC html for a checkbox form element.
 *
 * @see theme_checkbox()
 */
function dust_checkbox($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, ['id', 'name', '#return_value' => 'value']);

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, ['mdc-checkbox__native-control', 'form-checkbox']);

  return theme('mdc_checkbox', ['attributes' => $element['#attributes']]);
}

/**
 * Returns MDC html for a radio form element.
 *
 * @see theme_radio()
 */
function dust_radio($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'radio';
  element_set_attributes($element, [
    'id',
    'name',
    '#return_value' => 'value',
  ]);
  if (isset($element['#return_value']) && $element['#value'] !== FALSE
    && $element['#value'] == $element['#return_value']) {
    $element['#attributes']['checked'] = 'checked';
  }

  _form_set_class($element, ['mdc-radio__native-control']);

  return theme('mdc_radio', ['attributes' => $element['#attributes']]);
}

/**
 * Button element theme function.
 *
 * @see theme_button()
 */
function dust_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = $element['#type'];
  element_set_attributes($element, ['id', 'name', 'value']);

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = ['disabled', 'form-button-disabled'];
  }

  return theme('mdc_button', array(
    'label' => $element['#value'],
    'attributes' => $element['#attributes'],
    'icon' => '',
    'type' => $element['#button_style'] ?? $element['#button_type'],
  ));
}

/**
 * Preprocess MDC switch knob.
 *
 * @param array $vars
 *   An associative array containing:
 *   - attributes: Any additional attributes to add.
 *
 * @throws \Exception
 *
 * @see mdc-switch.tpl.php
 */
function template_preprocess_mdc_switch(array &$vars) {
  // Add switch knob js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_switch.js');
}

/**
 * Returns MDC html for a switch form element.
 */
function dust_switch($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array(
    'id',
    'name',
    '#return_value' => 'value',
  ));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('mdc-switch__native-control'));

  return theme('mdc_switch', ['attributes' => $element['#attributes']]);
}

/**
 * Implements template_preprocess_HOOK().
 *
 * @see theme_mdc_menu_tree_wrapper()
 */
function template_preprocess_mdc_menu_tree_wrapper(&$variables) {
  $variables['tree'] = $variables['tree']['#children'];
}

/**
 * Returns HTML for a wrapper for a MDC menu surface sub-tree.
 *
 * This menu tree wrapper is to help materialize drupal menu trees.
 * In order to use this wrapper you must specify the menu to do so
 * with for example hook_block_view_alter().
 * @code
 *  $data['content']['#theme_wrappers'] = ['theme_mdc_menu_tree_wrapper'];
 * @code
 *
 * @see https://github.com/material-components/material-components-web/tree/master/packages/mdc-menu
 */
function theme_mdc_menu_tree_wrapper($variables) {
  // Add attributes to provide accessibility and mdc classes.
  return theme('mdc_menu_surface', ['tree' => $variables['tree']]);
}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @see theme_menu_link()
 */
function dust_preprocess_menu_link(&$vars) {
  $element = &$vars['element'];
  $element['#attributes']['role'] = ['menuitem'];
  $element['#attributes']['class'] = ['mdc-list-item'];
}

/**
 * Preprocess MDC snackbar.
 *
 * @param array $vars
 *   An associative array containing:
 *   - icon: The snakcbar icon, if any.
 *   - attributes: Any additional attributes to add.
 *   - type: Type of snackbar to set such as 'leading'.
 *   - message: The snackbar message.
 *   - actions: The snackbar actions for example buttons.
 *
 * @throws \Exception
 *
 * @see mdc-snackbar.tpl.php
 */
function template_preprocess_mdc_snackbar(array &$vars) {
  // Add js MDC snackbar js & css.
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/snackbar.css');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_snackbar.js');
  $type = $vars['type'];
  $attributes = $vars['attributes'];
  // Default attributes.
  $attributes['class'][] = 'mdc-snackbar';

  // Add class if type leading.
  if (isset($type) && is_string($type)) {
    if ($type == 'leading') {
      $attributes['class'][] = 'mdc-snackbar--leading';
    }
    if ($type == 'open') {
      $attributes['class'][] = 'mdc-snackbar--open';
    }
  }

  $vars['attributes'] = $attributes;
}

/**
 * Preprocess MDC tab bar theme hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - attributes: Tab bar attributes.
 *   - stacked: Whether should tabs be stacked or no
 *   - tabs: An array of tabs. Each $tab in $tabs contains:
 *     - $tab['attributes']: The tab attributes.
 *     - $tab['label']: The tab label.
 *     - $tab['icon']: The tab icon.
 *     - $tab['active']: Whether tab is active or not.
 *     - $tab['indicator_classes']: The indicator classes.
 *
 * @throws \Exception
 *
 * @see mdc-tab-bar.tpl.php
 */
function template_preprocess_mdc_tab_bar(array &$vars) {

  // Attach tab bar js.
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_tab_bar.js');
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/tab-bar.css');

  // Default wrapper attributes.
  $wrapper_attributes = &$vars['attributes'];
  $wrapper_attributes['class'][] = 'mdc-tab-bar';
  $wrapper_attributes['role'] = 'tablist';
  $wrapper_attributes['aria-orientation'] = 'horizontal';

  $has_active_tab = FALSE;
  foreach ($vars['tabs'] as $i => &$tab) {

    // We won't output the tabs without icons and labels.
    if (!isset($tab['icon']) && !isset($tab['label']) && !isset($tab['link'])) {
      unset($vars['tabs'][$i]);
      continue;
    }

    // Add required attributes to the tab.
    $attributes = &$tab['attributes'];
    $tab_id = $attributes['id'] ?? drupal_html_id('mdc-tab');
    if (is_array($tab_id)) {
      $tab_id = reset($tab_id);
    }
    $tab_id .= '--tab';
    $tab['indicator_classes'][] = 'mdc-tab-indicator';

    // Special case for the disabled tab.
    if (isset($tab['disabled'])) {
      $attributes['class'][] = 'mdc-tab--disabled';
      $attributes['role'] = $attributes['role'] ?? 'tab';
      $attributes['id'] = $tab_id;
      $attributes['type'] = 'button';
      $attributes['tabindex'] = -1;
      $tab['indicator_classes'] = implode(' ', $tab['indicator_classes']);
      continue;
    }

    $attributes['class'][] = 'mdc-tab';
    $attributes['role'] = $attributes['role'] ?? 'tab';
    $attributes['id'] = $tab_id;
    $attributes['type'] = 'button';

    // If the tab bat should be stacked then we add a stacked class to the tab.
    if ($vars['stacked']) {
      $attributes['class'][] = 'mdc-tab--stacked';
    }

    // Add classes based on the tab active status.
    // Only one tab should be active, so we ignore all other active tabs if we
    // already have one active tab.
    if (isset($tab['active']) && $tab['active'] && !$has_active_tab) {
      $attributes['tabindex'] = 0;
      $attributes['aria-selected'] = 'true';

      $attributes['class'][] = 'mdc-tab--active';
      $tab['indicator_classes'][] = 'mdc-tab-indicator--active';
      $has_active_tab = TRUE;
    }
    else {
      $attributes['tabindex'] = -1;
      $attributes['aria-selected'] = 'false';
    }

    $tab['indicator_classes'] = implode(' ', $tab['indicator_classes']);
  }

  // Make first tab active if no active tab specified.
  if (!empty($vars['tabs']) && !$has_active_tab) {
    $key = array_key_first($vars['tabs']);
    $attributes = &$vars['tabs'][$key]['attributes'];
    $attributes['tabindex'] = 0;
    $attributes['aria-selected'] = 'true';

    $attributes['class'][] = 'mdc-tab--active';
    $vars['tabs'][$key]['indicator_classes'] .= ' mdc-tab-indicator--active';
  }
}

/**
 * Preprocess MDC list theme hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - attributes: Tab bar attributes.
 *   - items: Array of list items.
 *
 * @see mdc-list.tpl.php
 */
function template_preprocess_mdc_list(array &$vars) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_list.js');
  $items = &$vars['items'];
  $attributes = &$vars['attributes'];
  $type = &$vars['type'];
  $attributes['class'][] = 'mdc-list';
  $attributes['class'][] = 'dust-mdc-list';

  // Fallback to unordered list if something went wrong.
  if (!preg_match('/(ul|ol)/i', $type)) {
    $type = 'ul';
  }

  // Add mdc two line class if needed.
  if (array_column($items, 'secondary_text')) {
    $attributes['class'][] = 'mdc-list--two-line';
  }

  // Add default classes to the list items..
  $has_tab_index = FALSE;
  foreach ($items as &$item) {
    // Don't add default classes if a item is an divider.
    if (isset($item['attributes']['class']) && in_array('mdc-list-divider', $item['attributes']['class'], TRUE)) {
      continue;
    }
    if (!isset($item['attributes'])) {
      $item['attributes']['class'] = ['mdc-list-item'];
    }
    else {
      $item['attributes']['class'][] = 'mdc-list-item';
    }

    // Add tabindex 0 to all elements to enable tabbing on any of them.
    $item['attributes']['tabindex'] = '0';
  }

  // If we need to output the divider then we need to generate attributes for
  // the separator.
  if ($divider = $vars['divider']) {
    $divider_attributes = array();
    switch ($divider) {
      case 'padded':
        $divider_attributes['class'][] = 'mdc-list-divider--padded';
        break;

      case 'inset':
        $divider_attributes['class'][] = 'mdc-list-divider--inset';
        break;
    }
    $divider_attributes['class'][] = 'mdc-list-divider';
    $divider_attributes['role'] = 'separator';

    $vars['divider_attributes'] = $divider_attributes;
  }
}

/**
 * Table element theme function.
 *
 * Materialize tables.
 *
 * @see theme_table()
 */
function dust_table(array $variables) {
  // Add css.
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/data-table.css');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_data_table.js');

  $header = $variables['header'];
  $rows = $variables['rows'];
  $attributes = $variables['attributes'];
  $wrapper_attributes = $variables['wrapper_attributes'];
  $caption = $variables['caption'];
  $colgroups = $variables['colgroups'];
  $sticky = $variables['sticky'];
  $empty = $variables['empty'];
  if (isset($attributes['class']) && is_string($attributes['class'])) {
    $attributes['class'] = [$attributes['class']];
  }

  // Add sticky headers, if applicable.
  if (!empty($header) && $sticky) {
    drupal_add_js('misc/tableheader.js');

    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }

  // Add MDC table element class.
  $wrapper_attributes['class'][] = 'mdc-data-table';
  $wrapper_attributes['class'][] = 'dust-table';
  $attributes['class'][] = 'mdc-data-table__table';

  $output = '<div' . drupal_attributes($wrapper_attributes) . '><table' . drupal_attributes($attributes) . ">\n";
  if (isset($caption)) {
    $output .= '<caption>' . $caption . "</caption>\n";
  }

  // Format the table columns:
  if (!empty($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();

      // Check if we're dealing with a simple or complex column.
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }

      // Build colgroup.
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
    }
  }

  // Add the 'empty' row message if available.
  if (empty($rows) && $empty) {
    $header_count = 0;
    if (!empty($header)) {
      foreach ($header as $header_cell) {
        if (is_array($header_cell)) {
          $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
        }
        else {
          $header_count++;
        }
      }
    }
    $rows[] = array(
      array(
        'data' => $empty,
        'colspan' => $header_count,
        'class' => array(
          'empty',
          'message',
        ),
      ),
    );
  }

  // Format the table header.
  if (!empty($header)) {
    $ts = tablesort_init($header);

    // HTML requires that the thead tag has tr tags in it followed by tbody
    // tags. Using ternary operator to check and see if we have any rows.
    $output .= !empty($rows)
      ? ' <thead><tr class="mdc-data-table__header-row">'
      : ' <tr class="mdc-data-table__header-row">';

    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= dust_table_cell($cell, TRUE);
    }

    // Using ternary operator to close the tags based on whether
    // or not there are rows.
    $output .= !empty($rows) ? " </tr></thead>\n" : "</tr>\n";
  }
  else {
    $ts = array();
  }

  // Format the table and footer rows.
  $sections = array();
  if (!empty($rows)) {
    $sections['tbody'] = $rows;
  }
  if (!empty($variables['footer'])) {
    $sections['tfoot'] = $variables['footer'];
  }

  // Tbody and tfoot have the same structure and are built using the same
  // procedure.
  foreach ($sections as $tag => $content) {
    if ($tag === 'tbody') {
      $output .= "<{$tag} class=\"mdc-data-table__content\">\n";
    }
    else {
      $output .= "<{$tag}>\n";
    }
    $flip = array(
      'even' => 'odd',
      'odd' => 'even',
    );
    $class = 'even';
    $default_no_striping = $tag === 'tfoot';
    foreach ($content as $number => $row) {

      // Check if we're dealing with a simple or complex row.
      if (isset($row['data'])) {
        $cells = $row['data'];
        $no_striping = isset($row['no_striping']) ? $row['no_striping'] : $default_no_striping;

        // Set the attributes array and exclude 'data' and 'no_striping'.
        $attributes = $row;
        unset($attributes['data']);
        unset($attributes['no_striping']);
      }
      else {
        $cells = $row;
        $attributes = array();
        $no_striping = $default_no_striping;
      }
      if (!empty($cells)) {
        $attributes['class'][] = 'mdc-data-table__row';
        $attributes['aria-selected'] = 'false';
        // Add odd/even class.
        if (!$no_striping) {
          $class = $flip[$class];
          $attributes['class'][] = $class;
        }

        // Build row.
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= dust_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "</" . $tag . ">\n";
  }
  $output .= "</table></div>\n";

  return $output;
}

/**
 * Returns MDC HTML output for a single table cell for theme_table().
 *
 * @param array $cell
 *   Array of cell information, or string to display in cell.
 * @param bool $header
 *   TRUE if this cell is a table header cell, FALSE if it is an ordinary
 *   table cell. If $cell is an array with element 'header' set to TRUE, that
 *   will override the $header parameter.
 *
 * @return string
 *   HTML for the cell.
 */
function dust_table_cell(array $cell, $header = FALSE) {
  if (is_array($cell)) {
    $data = $cell['data'] ?? '';
    // Cell's data property can be a string or a renderable array.
    if (is_array($data)) {
      $data = drupal_render($data);
    }
    $header |= isset($cell['header']);
    unset($cell['data'], $cell['header']);
    if (!isset($cell['class'])) {
      $cell['class'] = array();
    }
    if (is_string($cell['class'])) {
      $cell['class'] = [$cell['class']];
    }
    // Add mdc table cell classes.
    if ($header) {
      $cell['class'][] = 'mdc-data-table__header-cell';
      $cell['role'] = 'columnheader';
      $cell['scope'] = 'col';
    }
    else {
      $cell['class'][] = 'mdc-data-table__cell';
    }
    if (strpos($data, 'form-type-checkbox') !== FALSE) {
      $cell['class'][] = $header ? 'mdc-data-table__header-cell--checkbox' : 'mdc-data-table__cell--checkbox';
    }
    // Add attributes.
    $attributes = drupal_attributes($cell);
  }
  else {
    $attributes = array();
    // Add mdc table cell classes.
    if ($header) {
      $attributes['class'][] = 'mdc-data-table__header-cell';
      $attributes['role'] = 'columnheader';
      $attributes['scope'] = 'col';
    }
    else {
      $attributes['class'][] = 'mdc-data-table__cell';
    }
    if (strpos($cell, 'form-type-checkbox') !== FALSE) {
      $attributes['class'][] = $header ? 'mdc-data-table__header-cell--checkbox' : 'mdc-data-table__cell--checkbox';
    }
    // Add attributes.
    $attributes = drupal_attributes($attributes);
    $data = $cell;
  }

  if ($header) {
    $output = '<th' . $attributes . '>' . $data . '</th>';
  }
  else {
    $output = '<td' . $attributes . '>' . $data . '</td>';
  }

  return $output;
}

/**
 * Display a view as a table style.
 */
function dust_preprocess_views_view_table(&$vars) {
  // Add css.
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/data-table.css');
}

/**
 * Element validate for rangefield form element.
 *
 * @param array $element
 *   Range field form element.
 * @param array $form_state
 *   Form state.
 */
function dust_rangefield_element_validate(array $element, array &$form_state) {
  if (!is_numeric($element['#value'])) {
    form_error($element, t('%name must be numeric.', array('%name' => $element['#title'])));
    return;
  }
  if ($element['#value'] < $element['#min']) {
    form_error($element, t('%name value is below allowed.', array('%name' => $element['#title'])));
  }
  if ($element['#value'] > $element['#max']) {
    form_error($element, t('%name value is above allowed.', array('%name' => $element['#title'])));
  }
  if ($element['#value'] % $element['#step'] !== 0) {
    form_error($element, t('%name value is invalid.', array('%name' => $element['#title'])));
  }
}

/**
 * Preprocess MDC slider.
 *
 * @param array $variables
 *   An array containing 'element' with following keys:
 *     - #max: highest allowed value.
 *     - #min: lowest allowed value.
 *     - #step: step to quantize values by.
 *     - #discrete: displays the exact value of the range on the knob.
 *     - #display_markers: displays individual step markers.
 *
 * @see mdc-slider.tpl.php
 */
function template_preprocess_mdc_slider(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_slider.js');
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/slider.css');

  // Pass configs to aria attributes for js script.
  $element = $variables['element'];
  $variables['attributes'] = array(
    'class' => array('mdc-slider'),
    'aria-valuemax' => $element['#max'],
    'aria-valuemin' => $element['#min'],
    'aria-valuenow' => $element['#value'],
    'data-step' => $element['#step'],
    'aria-disabled' => $element['#disabled'],
    'aria-labelledby' => $element['#id'] . '-label',
    'data-input' => $element['#id'],
  );
  $variables['attributes'] = array_filter($variables['attributes']);

  $variables['discrete'] = FALSE;
  $variables['display_markers'] = FALSE;
  if (!empty($element['#discrete'])) {
    $variables['discrete'] = TRUE;
    $variables['attributes']['class'][] = 'mdc-slider--discrete';
  }
  if (!empty($element['#display_markers'])) {
    $variables['display_markers'] = TRUE;
    $variables['attributes']['class'][] = 'mdc-slider--display-markers';
  }

  // Default attributes for hidden input.
  $variables['input_attributes']['type'] = 'hidden';
  $variables['input_attributes']['name'] = $element['#name'];
  $variables['input_attributes']['id'] = $element['#id'];
  $variables['input_attributes']['value'] = $element['#value'];
  $variables['input_attributes']['aria-hidden'] = 'true';
}

/**
 * Preprocess MDC image list theme hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - attributes: Tab bar attributes.
 *   - items: Array of image attributes to render.
 *   - mansonry_layout: To render mansonry type.
 *
 *   @code
 *   $vars['items'] = [
 *     0 => [
 *       'path' => $path,
 *       'alt' => $alt,
 *       'label' => $label,
 *     ],
 *   ];
 * @code
 *
 * @throws \Exception
 *
 * @see mdc-card.tpl.php
 */
function template_preprocess_mdc_image_list(array &$vars) {
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/image-list.css');

  $items = &$vars['items'];
  $attributes = $vars['attributes'];
  $attributes['class'][] = 'mdc-image-list';
  $attributes['class'][] = 'dust-image-list';

  foreach ($items as $key => $item) {
    $items[$key]['image'] = theme('image', array(
      'path' => $item['path'],
      'alt' => $item['alt'] ?? '',
      'attributes' => array(
        'class' => array(
          'mdc-image-list__image',
        ),
        'role' => 'image',
      ),
    ));
  }

  // Add specific classes for mansory variant.
  if ($vars['mansonry_layout']) {
    $attributes['class'][] = 'mdc-image-list--masonry';
    $attributes['class'][] = 'mdc-image-list--with-text-protection';
  }

  // Add attributes.
  $vars['attributes'] = $attributes;
}

/**
 * Arranges fieldsets into groups.
 *
 * @param array $element
 *   An associative array containing the properties and children of the
 *   fieldset. Note that $element must be taken by reference here, so processed
 *   child elements are taken over into $form_state.
 * @param array $form_state
 *   The $form_state array for the form this fieldset belongs to.
 *
 * @return array
 *   The processed element.
 */
function dust_form_process_fieldset(array &$element, array &$form_state) {
  $parents = implode('][', $element['#parents']);

  // Each fieldset forms a new group. The #type 'vertical_tabs' basically only
  // injects a new fieldset.
  $form_state['groups'][$parents]['#group_exists'] = TRUE;
  $element['#groups'] = &$form_state['groups'];

  // Process vertical tabs group member fieldsets.
  if (isset($element['#group'])) {
    // Add this fieldset to the defined group (by reference).
    $group = $element['#group'];
    $form_state['groups'][$group][] = &$element;
  }

  // Contains form element summary functionalities.
  $element['#attached']['library'][] = array('system', 'drupal.form');

  // The .form-wrapper class is required for #states to treat fieldsets like
  // containers.
  if (!isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = array();
  }

  return $element;
}

/**
 * Returns HTML for a fieldset form element and its children.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 *     Properties used: #attributes, #children, #collapsed, #collapsible,
 *     #description, #id, #title, #value.
 *
 * @return string
 *   The fieldset html.
 */
function dust_fieldset(array $variables) {
  $element = $variables['element'];
  element_set_attributes($element, ['id']);
  _form_set_class($element, ['form-wrapper']);

  // Add attributes.
  $element['#attributes']['class'][] = 'dust-fieldset';

  // Collapsible fieldsets.
  if (!empty($element['#collapsible'])) {
    drupal_add_library('system', 'drupal.collapse');
    drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_fieldset.js');
    $element['#attributes']['class'][] = 'collapsible';
    if (!empty($element['#collapsed'])) {
      $element['#attributes']['class'][] = 'collapsed';
    }
  }

  $output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<legend><span class="fieldset-legend">' . $element['#title'] . '</span></legend>';
  }
  $output .= '<div class="fieldset-wrapper">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</fieldset>\n";
  return $output;
}

/**
 * Preprocess for textfield.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_textfield(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_textfield.js');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_line_ripple.js');

  $variables['outline'] = FALSE;
  $variables['full_width'] = FALSE;

  // Sets attributes from element properties as Drupal core does.
  $element = &$variables['element'];
  if (empty($element['#attributes']['type'])) {
    $element['#attributes']['type'] = 'text';
  }
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'size',
    'minlength',
    'maxlength',
    'placeholder',
    'pattern',
  ));
  _form_set_class($element, array('form-text', 'mdc-text-field__input'));

  // Adds extra hidden input for autocomplete.
  $variables['extra'] = '';
  if (isset($element['#wrapper_attributes'])) {
    $variables['wrapper_attributes'] = $element['#wrapper_attributes'];
  }
  if ($element['#autocomplete_path'] && !empty($element['#autocomplete_input'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';
    $element['#attributes']['aria-expanded'] = 'false';
    $element['#attributes']['aria-haspopup'] = 'true';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#autocomplete_input']['#id'];
    $attributes['value'] = $element['#autocomplete_input']['#url_value'];
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $variables['extra'] = '<input' . drupal_attributes($attributes) . ' />';
  }

  if (!empty($element['#outline'])) {
    $variables['outline'] = TRUE;
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--outlined';
  }
}

/**
 * Preprocess for password.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_password(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_textfield.js');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_line_ripple.js');

  $variables['outline'] = FALSE;
  $variables['full_width'] = FALSE;

  // Sets attributes from element properties as Drupal core does.
  $element = &$variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'minlength', 'maxlength', 'placeholder', 'pattern'));
  _form_set_class($element, array('form-text', 'mdc-text-field__input'));

  if (!empty($element['#outline'])) {
    $variables['outline'] = TRUE;
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--outlined';
  }
}

/**
 * Preprocess for textarea.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_textarea(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_textfield.js');

  $variables['outlined'] = $variables['element']['#outlined'] ?? TRUE;
  $variables['full_width'] = FALSE;
  $variables['wrapper_attributes']['class'][] = 'mdc-text-field--textarea';
  $variables['wrapper_attributes']['class'][] = 'form-textarea-wrapper';

  // Sets attributes from element properties as Drupal core does.
  $element = &$variables['element'];
  $variables['value'] = $element['#value'] ?? '';
  element_set_attributes($element, array(
    'id',
    'name',
    'cols',
    'rows',
    'maxlength',
    'placeholder',
  ));
  _form_set_class($element, array('form-textarea', 'mdc-text-field__input'));

  // Add resizable behavior.
  if (!empty($element['#resizable'])) {
    drupal_add_library('system', 'drupal.textarea');
    $variables['wrapper_attributes']['class'][] = 'resizable';
  }
}

/**
 * Preprocess for date and time themings.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_datetime(array &$variables) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_textfield.js');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_line_ripple.js');

  $variables['outline'] = FALSE;
  $variables['full_width'] = FALSE;

  // Sets attributes from element properties as Drupal core does.
  $element = &$variables['element'];
  element_set_attributes($element, array(
    'id',
    'name',
    'value',
    'max',
    'min',
    'type',
  ));
  _form_set_class($element, array('form-text', 'mdc-text-field__input'));

  if (!empty($element['#outline'])) {
    $variables['outline'] = TRUE;
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--outlined';
  }
}

/**
 * Replacement of form_process_date().
 *
 * @param array $element
 *   Date form element.
 *
 * @return array
 *   Date form element.
 */
function dust_form_process_date(array $element) {
  // Default to current date.
  if (empty($element['#value'])) {
    $element['#value'] = array(
      'day' => format_date(REQUEST_TIME, 'custom', 'j'),
      'month' => format_date(REQUEST_TIME, 'custom', 'n'),
      'year' => format_date(REQUEST_TIME, 'custom', 'Y'),
    );
  }

  // Format date value in a way input type date expects it.
  $element['#value'] = implode('-', array(
    $element['#value']['year'],
    $element['#value']['month'],
    $element['#value']['day'],
  ));
  return $element;
}

/**
 * Value callback for date textfield.
 *
 * @param array $element
 *   Form API element.
 * @param string|null $input
 *   Value from input.
 *
 * @return array
 *   Date formatted as in Drupal core.
 */
function dust_form_date_value_callback(array $element, $input = NULL) {
  if ($input && is_string($input) && (drupal_strrpos($input, '-') !== -1)) {
    [$year, $month, $day] = explode('-', $input);
    return array(
      'year' => $year,
      'month' => $month,
      'day' => $day,
    );
  }
  elseif (!empty($element['#default_value'])) {
    return $element['#default_value'];
  }
  return array();
}

/**
 * Returns HTML for a table with radio buttons or checkboxes.
 *
 * This is a copy of a theme_tableselect() function but here we add a checkbox
 * to the header and don't attach the JS.
 *
 * @see theme_tableselect()
 */
function dust_tableselect($variables) {
  $element = $variables['element'];
  $rows = array();
  $header = $element['#header'];
  if (!empty($element['#options'])) {
    // Generate a table row for each selectable item in #options.
    foreach (element_children($element) as $key) {
      $row = array();

      $row['data'] = array();
      if (isset($element['#options'][$key]['#attributes'])) {
        $row += $element['#options'][$key]['#attributes'];
      }
      // Render the checkbox / radio element.
      $row['data'][] = drupal_render($element[$key]);

      // As theme_table only maps header and row columns by order, create the
      // correct order by iterating over the header fields.
      foreach ($element['#header'] as $fieldname => $title) {
        // A row cell can span over multiple headers, which means less row cells
        // than headers could be present.
        if (isset($element['#options'][$key][$fieldname])) {
          // A header can span over multiple cells and in this case the cells
          // are passed in an array. The order of this array determines the
          // order in which they are added.
          if (!isset($element['#options'][$key][$fieldname]['data']) && is_array($element['#options'][$key][$fieldname])) {
            foreach ($element['#options'][$key][$fieldname] as $cell) {
              $row['data'][] = $cell;
            }
          }
          else {
            $row['data'][] = $element['#options'][$key][$fieldname];
          }
        }
      }
      $rows[] = $row;
    }
    // Add an empty header or a "Select all" checkbox to provide room for the
    // checkboxes/radios in the first table column.
    if ($element['#js_select']) {
      // Add a "Select all" checkbox.
      $checkbox = array(
        '#type' => 'checkbox',
      );
      array_unshift($header, drupal_render($checkbox));
    }
    else {
      // Add an empty header when radio buttons are displayed or a "Select all"
      // checkbox is not desired.
      array_unshift($header, '');
    }
  }
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => $element['#empty'],
    'attributes' => $element['#attributes'],
  ));
}

/**
 * Common preprocess for text based fields.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_textfields(array &$variables) {
  $element = &$variables['element'];
  $variables['attributes'] = $element['#attributes'];
  $variables['wrapper_attributes']['class'][] = 'mdc-text-field';

  // Set label and attributes for it.
  if ($element['#title_display'] !== 'none' && !empty($element['#title'])) {
    $variables['label'] = filter_xss_admin($element['#title']);
    $variables['label_attributes'] = array(
      'for' => $variables['attributes']['id'],
      'class' => ['mdc-floating-label'],
      'id' => 'label-for-' . $variables['attributes']['id'],
    );
  }
  else {
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--no-label';
    if (!empty($element['#title'])) {
      $variables['attributes']['aria-label'] = filter_xss_admin($element['#title']);
      if (empty($variables['attributes']['placeholder'])) {
        $variables['attributes']['placeholder'] = filter_xss_admin($element['#title']);
      }
    }
  }

  if (!empty($element['#full_width'])) {
    $variables['full_width'] = TRUE;
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--fullwidth';
    if (isset($variables['label'])) {
      $variables['attributes']['aria-label'] = $variables['label'];
      if (!isset($variables['attributes']['placeholder'])) {
        $variables['attributes']['placeholder'] = $variables['label'];
      }
    }
  }

  // Set attributes and classes based on element configs.
  if (!empty($element['#value']) && !isset($element['#format'])) {
    $variables['label_attributes']['class'][] = 'mdc-floating-label--float-above';
  }
  if (!empty($element['#required'])) {
    $variables['attributes']['required'] = 'true';
  }
  if (!empty($element['#disabled'])) {
    $variables['attributes']['disabled'] = 'true';
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--disabled';
  }
  $element['#parents'] = $element['#parents'] ?? [];
  if (form_get_error($element) && !empty($element['#validated'])) {
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--invalid';
  }
  if (!empty($element['#description'])) {
    $variables['help_text'] = filter_xss_admin($element['#description']);
    $variables['help_attributes'] = array(
      'class' => array(
        'mdc-text-field-helper-text',
        'mdc-text-field-helper-text--persistent',
      ),
      'id' => $variables['attributes']['id'] . '-helper-text',
    );
    $variables['attributes']['aria-describedby'] = $variables['attributes']['id'] . '-helper-text';
    $variables['attributes']['aria-controls'] = $variables['attributes']['id'] . '-helper-text';
  }
  if (!empty($element['#icon']) && !$variables['full_width']) {
    $variables['icon'] = $element['#icon'];
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--with-leading-icon';
  }
  if (!empty($element['#trailing_icon']) && !$variables['full_width']) {
    $variables['trailing_icon'] = $element['#trailing_icon'];
    $variables['wrapper_attributes']['class'][] = 'mdc-text-field--with-trailing-icon';
  }
  if (!empty($element['#maxlength'])) {
    $variables['maxlength'] = $element['#maxlength'];
  }
}

/**
 * Returns whether form field is handled by MDC textfield.
 *
 * @param string|null $type
 *   Form field type.
 *
 * @return bool
 *   TRUE if form field type is handled by MDC textfield.
 */
function _dust_form_field_is_textfield($type = NULL) {
  $types = array(
    'textfield',
    'textarea',
    'password',
    'time',
    'date',
    'machine_name',
    'webform_email',
  );
  if (isset($type) && in_array($type, $types)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Mdc select preprocess hook.
 *
 * @param array $vars
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_mdc_select(array &$vars) {
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_line_ripple.js');
  drupal_add_js(drupal_get_path('theme', 'dust') . '/js/dust_mdc_select.js');
  drupal_add_css(drupal_get_path('theme', 'dust') . '/css/select.css');

  // Add regular select to hide it and update on material select change.
  $select_vars = $vars;
  $select_vars['element']['#attributes']['class'][] = 'mdc-select__value';
  $select_vars['element']['#attributes']['aria-hidden'] = 'true';
  $vars['select'] = dust_select($select_vars);

  $element = &$vars['element'];
  $element['#attributes']['class'][] = 'mdc-select';
  $element['#attributes']['class'][] = 'dust-mdc-select';
  _form_set_class($element, ['form-select']);

  // Set attributes and classes based on element configs.
  $vars['anchor_attributes'] = ['class' => ['mdc-select__anchor']];
  $vars['label_attributes'] = [
    'class' => [
      'mdc-floating-label',
    ],
    'id' => drupal_html_id('select-label-' . $element['#id']),
  ];
  $vars['selected_text_attributes'] = array(
    'class' => array(
      'mdc-select__selected-text',
    ),
    'id' => drupal_html_id('selected-text-' . $element['#id']),
  );
  if (empty($element['#title'])) {
    $vars['selected_text_attributes']['class'][] = 'no-label';
  }
  // Clear empty option.
  if (isset($element['#options'][NULL])) {
    $element['#options'][NULL] = '';
  }

  if (!empty($element['#value'])) {
    $vars['value'] = (string) (is_array($element['#value']) ? reset($element['#value']) : $element['#value']);
  }
  else {
    $vars['value'] = !empty($element['#empty_value']) ? $element['#empty_value'] : '';
  }
  $vars['selected'] = array_key_exists($vars['value'], $element['#options'])
    ? $element['#options'][$vars['value']]
    : NULL;

  if (!empty($element['#required'])) {
    $element['#attributes']['required'] = 'true';
    $element['#attributes']['class'][] = 'mdc-select--required';
    $vars['anchor_attributes']['aria-required'] = 'true';
  }
  if (!empty($element['#disabled'])) {
    $element['#attributes']['disabled'] = 'true';
    $element['#attributes']['class'][] = 'mdc-select--disabled';
  }
  if (!empty($element['#outlined'])) {
    $vars['outlined'] = TRUE;
    $element['#attributes']['class'][] = 'mdc-select--outlined';
  }

  // TODO: Implement anchor top using menu-anchor-corner data attribute.
  if (!empty($element['#anchor_top'])) {
    $element['#attributes']['class'][] = 'dust-anchor-top';
  }

  if (!empty($element['#trailing_icons'])) {
    $icons = array();
    foreach ($element['#trailing_icons'] as $key => $icon) {
      $icons[$key] = render($icon);
    }
    $element['#attributes']['data-trailing-icons'] = drupal_json_encode($icons);
  }

  if (form_get_error($element) && !empty($element['#validated'])) {
    $variables['wrapper_attributes']['class'][] = 'mdc-select--invalid';
  }
}

/**
 * Returns HTML for a sort icon.
 *
 * @param array $variables
 *   An associative array containing:
 *   - style: Set to either 'asc' or 'desc', this determines which icon to
 *     show.
 *
 * @return string
 *   Table sort indicator markup.
 *
 * @throws \Exception
 */
function dust_tablesort_indicator(array $variables) {
  $title = t('sort descending');
  $icon = 'arrow_upward';

  if ($variables['style'] === 'asc') {
    $title = t('sort ascending');
    $icon = 'arrow_downward';
  }
  return theme('html_tag', array(
    'element' => array(
      '#tag' => 'i',
      '#value' => $icon,
      '#attributes' => array(
        'class' => array(
          'dust-sort-icon',
          'material-icons',
        ),
        'title' => $title,
      ),
    ),
  ));
}

/**
 * Process function for webform email component.
 *
 * @param array $element
 *   Webform email form element.
 *
 * @return array
 *   The element array.
 */
function dust_webform_email_process(array $element) {
  $element['#attributes']['type'] = 'email';
  $element['#autocomplete_path'] = FALSE;
  $element['#placeholder'] = $element['#webform_placeholder'] ?? NULL;

  if (!empty($element['#attributes']['readonly'])) {
    $element['#disabled'] = TRUE;
    unset($element['#attributes']['readonly']);
  }

  // If user selected long format disable browser validation for email value.
  $format = webform_variable_get('webform_email_address_format') == 'long'
    ? $element['#webform_component']['extra']['format']
    : 'short';
  if ($format === 'long') {
    unset($element['#attributes']['type']);
  }
  return $element;
}

/**
 * Preprocess for webform element.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function dust_preprocess_webform_element(array &$variables) {
  $element = &$variables['element'];
  $element_type = isset($element['#type']) ? $element['#type'] : '';

  // We don't need to render description twice for textfields because it's
  // been moved to help text.
  // @see template_preprocess_mdc_textfields()
  if (_dust_form_field_is_textfield($element_type)) {
    $element['#description'] = NULL;
  }
}

/**
 * Returns HTML for a select form element.
 *
 * This is a copy of a theme_select() function but here we fix bug
 * with options and option groups that exist at the same time.
 *
 * @see theme_select()
 */
function dust_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, ['id', 'name', 'size']);
  _form_set_class($element, ['form-select']);

  // Detect select with options and option groups at the same time.
  $o_ex = $og_ex = FALSE;
  foreach ($element['#options'] as $option) {
    if (is_array($option)) {
      $og_ex = TRUE;
    }
    else {
      $o_ex = TRUE;
    }
  }

  // Generate new group for options.
  if ($o_ex && $og_ex) {
    $empty_group = [];
    foreach ($element['#options'] as $key => $option) {
      if (!is_array($option)) {
        $empty_group[$key] = $option;
        unset($element['#options'][$key]);
      }
    }
    $element['#options'] = array_merge([t('- No group -') => $empty_group], $element['#options']);
  }

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
}
